<?php
/**
 * This is an array of post types passed to the sitemapper. As you add new post types, list them here if you want them to be sitemapped.
 * @var array
 */
$sitemap_posts = array('post', 'page', 'event', 'faq', 'tour');
$robots_disallow = array('gallery', 'user');
$unit_query = "SELECT * FROM wp_building_units";
/**
 * An array of AFC field names with content you want to be searchable with wp search. Data type Strings.
 * @var array
 */
$custom_fields = array('');

define('TEMPLATEPATH', get_template_directory());

/**
 * Require the built-in Timber plugin, server-side mobile detection, the XML sitemap generator, and the php used for new admin functionality.
 * Mobile detect usage:
 * $var = new Mobile_Detect();
 * $var->isMobile(), $var->isTablet(), etc. See documentation for full features.
 * TODO Find a better way to include the plugin. Currently the plugin will not show up in the WP admin
 */
require_once get_template_directory() . '/plugins/timber-library/timber.php';
require_once get_template_directory() . '/plugins/advanced-custom-fields-pro/acf.php';
require_once get_template_directory() . '/plugins/wp-nested-pages/nestedpages.php';

require_once get_template_directory() . '/php/Auth.php';
require_once get_template_directory() . '/php/Breadcrumbs.php';
require_once get_template_directory() . '/php/Events.php';
require_once get_template_directory() . '/php/Faqs.php';
// require_once get_template_directory() . '/php/Galleries.php';
require_once get_template_directory() . '/php/Image_Utils.php';
require_once get_template_directory() . '/php/Members.php';
require_once get_template_directory() . '/php/Mobile_Detect.php';
require_once get_template_directory() . '/php/Page_Mapping.php';
require_once get_template_directory() . '/php/Sitemap_XML.php';
require_once get_template_directory() . '/php/Sort_Posts.php';
require_once get_template_directory() . '/php/Tours_Widget.php';
require_once get_template_directory() . '/php/Tours.php';
// require_once get_template_directory() . '/php/Units.php';

require_once get_template_directory() . '/php/admin.php';
require_once get_template_directory() . '/php/acf-fields.php';
require_once get_template_directory() . '/php/acf-search.php';

use Timber\FunctionWrapper;

/**
 * Set up the AFC path to be our built in plugin path
 * @param  string $path path to be overwritten
 * @return string       The full path of the new path... yes that makes sense
 */
function my_acf_settings_path($path){
	$path = get_template_directory() . '/plugins/advanced-custom-fields-pro/';

	return $path;
}
add_filter('acf/settings/path', 'my_acf_settings_path');

/**
 * Set up the AFC dir to be our built in plugin dir
 * @param  string $dir dir to be overwritten
 * @return string       The full path of the new dir
 */
function my_acf_settings_dir($dir){
	$dir = get_template_directory_uri() . '/plugins/advanced-custom-fields-pro/';

	return $dir;
}
add_filter('acf/settings/dir', 'my_acf_settings_dir');

/* uncomment this to hide ACF from the wp-admin */
//add_filter('acf/settings/show_admin', '__return_false');

/**
 * Basic theme stuff here. Curretly just adds thumbnail support to posts
 */
function mvnp_basic_setup(){
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
}
add_action('after_setup_theme', 'mvnp_basic_setup');

/**
 * Load text domain for i18n translations
 */
function mvnp_basic_languages(){
	$mo_path = get_template_directory() . '/languages/';
	if(file_exists($mo_path . get_locale() . '.mo')){
		load_textdomain('mvnp_basic', $mo_path . get_locale() . '.mo');
	}
}
add_action('after_setup_theme', 'mvnp_basic_languages');

/**
 * Sets the name of the php template used for the current page to $GLOBALS. Used to load page-specific styles and scripts
 * @param string $template php template name, ending in .php
 * @return string the template name, unmodified
 */
function var_template_include($template){
	$GLOBALS['current_theme_template'] = basename($template);
	return $template;
}
add_filter('template_include', 'var_template_include');

/**
 * Load scripts, but first get rid of the built-in WP version of jQuery.
 * Our concatinated, minified script includes jQuery and any other 3rd party scripts you wish to include
 * Google maps with our API key is included. Can be used for dev, but probably should be changed to a client account for prod.
 *
 * Recaptcha script is also included and disabled. By default, the signup is in a modal so the script is loaded
 * async when that modal is created. If your signup is loaded normally on the page, uncomment the script.
 */
function mvnp_basic_load_scripts(){
	wp_deregister_script('jquery');
	wp_enqueue_script('google_maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDUmWrI564wWhoAnJmync64ZZPOHAYe1Ac', false, '1.0', true);
	//wp_enqueue_script('google_recaptcha', 'https://www.google.com/recaptcha/api.js');
	wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', false, null, true);

	if(file_exists(get_template_directory() . '/js/app/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.js')){
		wp_enqueue_script('page_js', get_template_directory_uri() . '/js/app/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.js', 'main_js', null, true);
	}
}
add_action('wp_enqueue_scripts', 'mvnp_basic_load_scripts');

/**
 * Load our styles. Lets keep this minimal.
 */
function mvnp_basic_load_styles(){
	wp_enqueue_style('main_style', get_template_directory_uri() . '/style.css', false, null, 'screen');
	wp_style_add_data('main_style', 'rtl', 'replace');
	global $wp_styles;

	if(file_exists(get_template_directory() . '/css/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.css')){
		wp_enqueue_style('page_style', get_template_directory_uri() . '/css/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.css', false, null, 'screen');
		$wp_styles->add_data('page_style', 'disabled', TRUE);
	}
	$wp_styles->add_data('main_style', 'disabled', TRUE);
}
add_action('wp_enqueue_scripts', 'mvnp_basic_load_styles');

/**
 * Turn off the stupid wordpress emojis. They wouldnt be so bad but they grab all your HTML entities and replace them with img tags. Lame city.
 */
function disable_wp_emojicons(){
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
}
add_action('init', 'disable_wp_emojicons');

/**
 * Create custom image sizes.
 * Use add_image_size( 'thumbnail_size_name', width, height, array( 'horiz_scale_origin', 'vert_scale_origin' ) );
 */
function mvnp_basic_custom_image_sizes(){

}
add_action('after_setup_theme', 'mvnp_basic_custom_image_sizes');

/**
 * Removes the admin bar from the front end for chump users with no role.
 */
function remove_admin_bar(){
	if(wp_get_current_user()->roles[0] == ''){
		show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');

/**
 * Adds custom messages to the WP admin
 * @param  string $msg The message you want to display
 * @param  string $lvl The level (or color) of the notice. Valid vals are 'notice-error', 'notice-warning', 'notice-success', and 'notice-info'
 */
function mvnp_basic_notice($msg = 'no message', $lvl = 'notice-info'){
	?>
	<div class="notice <?php echo $lvl; ?>">
		<p><?php echo $msg; ?></p>
	</div>
<?php
}

/**
 * Gets the path of the user's private upload directory. Located in uploads, its their nicename
 * @param  stdObj $user wordpress user
 * @return str       the user's upload path
 */
function get_user_dir($user){
	$upload_dir = wp_upload_dir();
	$subdir = $user->user_nicename;
	$modified['subdir'] = $subdir;
	$modified['url'] = $upload_dir['baseurl'] . '/' . $subdir;
	$modified['path'] = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . $subdir;

	return $modified;
}

/**
 * group an array of objects by a key
 * @param  array  $array array to be grouped
 * @param  string $key   object key to group by
 * @return array         grouped array
 */
function group_by_object_key($array, $key){
	$grouped_array = array();

	foreach($array as $item){
		$grouped_array[$item->$key][] = $item;
	}

	return $grouped_array;
}

/**
 * filter an array of objects by key/value
 * @param  array  $array array to be filtered
 * @param  string $key   object key to look at
 * @param  string $value value of key to filter by
 * @return array         filtered array
 */
function filter_by_object_value($array, $key, $value){
	$filtered_array = array();

	foreach($array as $item){
		if (isset($item->$key)){
			if(strtolower($item->$key) == strtolower($value)){
				$filtered_array[] = $item;
			}
		}
	}

	return $filtered_array;
}

/**
 * A simple post sorter.
 * @param  array  $posts   An array of the posts you need to sort
 * @param  string  $orderby The field you want to or by
 * @param  string  $order   ASC or DESC
 * @param  boolean $unique  When true, the sort will also remove duplicate post
 * @return array           Sorted list of posts
 */
function sort_posts($posts, $orderby, $order = 'ASC', $unique = true){
	if(!is_array($posts)){
		return false;
	}

	usort($posts, array(new Sort_Posts($orderby, $order), 'sort'));

	// use post ids as the array keys
	if($unique && count($posts)){
		$posts = array_combine(wp_list_pluck($posts, 'ID'), $posts);
	}

	return $posts;
}

/**
 * Adds menu locations to the menu manager
 */
function make_menus(){
	register_nav_menus(array(
		'main_navigation' => __('Main Navigation', 'mvnp_basic_admin'),
		'footer_navigation' => __('Footer Navigation', 'mvnp_basic_admin'),
	));
}
add_action('after_setup_theme', 'make_menus');

/**
 * Adds widget locations to the widget manager
 */
function mvnp_basic_widgets_init(){
	register_sidebar(array(
		'name' => __('Footer Call to Action', 'mvnp_basic_admin'),
		'id' => 'footer-cta',
		'before_widget' =>
<<<EOT
			<section id="call-to-action">
				<div class="container">
					<div class="row clearfix">
						<div class="col-sm-12">
EOT
		,'after_widget' =>
<<<EOT
						</div>
					</div>
				</div>
			</section>
EOT
		,'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name' => __('Sidebar Call to Action', 'mvnp_basic_admin'),
		'id' => 'sidebar-cta',
		'before_widget' =>
<<<EOT
			<section id="sidebar-call-to-action">
				<div class="container">
					<div class="row clearfix">
						<div class="col-sm-12">
EOT
		,'after_widget' =>
<<<EOT
						</div>
					</div>
				</div>
			</section>
EOT
		,'before_title' => '<h5>',
		'after_title' => '</h5>',
	));
}
add_action('widgets_init', 'mvnp_basic_widgets_init');

function register_widgets() {
	register_widget('Tours_Widget');
}
add_action( 'widgets_init', 'register_widgets' );

/**
 * redirects to /search instead of using url params
 */
function mvnp_basic_search_url(){
	if(is_search() && !empty($_GET['s'])){
		wp_redirect(home_url('/search/') . urlencode(get_query_var('s')));
		exit();
	}
}
add_action('template_redirect', 'mvnp_basic_search_url');

/**
 * Add global options to the timber context. Currrently adds new menu and widget locations.
 * It also adds the sidebar. If you want to disable the sidebar globally, remove the '$data['sidebar']' line
 * @param Object $data The Timber context
 * @return Object The Timber context
 */
function add_to_context($data){
	global $current_user;

	$data['main_menu'] = new TimberMenu('main_navigation');
	$data['footer_menu'] = new TimberMenu('footer_navigation');
	$data['breadcrumbs'] = Breadcrumbs::create('&#x221f;', get_the_title(get_option('page_on_front')));
	$data['sidebar'] = false;
	$data['can_login'] = false;
	$data['is_logged_in'] = is_user_logged_in();
	$data['cta_widget'] = Timber::get_widgets('footer-cta');

	if(function_exists('qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage')){
		$data['available_languages'] = get_option('qtranslate_language_names');
	}

	if(is_user_logged_in()){
		$data['user'] = $current_user;
		$data['user_link'] = Members::member_permalink($current_user->data->user_nicename);
	}

	return $data;
}
add_filter('timber_context', 'add_to_context');

/**
 * Function for adding filters and functions to twig.
 * @param Object $twig The twig scope
 */
function add_to_twig($twig){
	$twig->addExtension(new Twig_Extension_StringLoader());
	$twig->addFilter(new Twig_SimpleFilter('get_webp', function ($url){
		if(isset($_SERVER['HTTP_USER_AGENT'])){
			$agent = $_SERVER['HTTP_USER_AGENT'];
		}

		if(strlen(strstr($agent, 'Chrome')) > 0){
			return str_ireplace(array('.jpg', '.jpeg', '.png', '.gif'), '.webp', $url);
		} else return $url;
	}));

	$twig->addFilter(new Twig_SimpleFilter('parse_url', function($url){
		if(!$url){
			return __('Empty string!', 'mvnp_basic');
		}

		return parse_url($url);
	}));

	$twig->addFilter(new Twig_SimpleFilter('preg_replace', function($subject, $pattern, $replacement){
		if(!$subject){
			return __('Empty string!', 'mvnp_basic');
		}

		return preg_replace($pattern, $replacement, $subject);
	}));

	$twig->addFilter(new Twig_SimpleFilter('error_log', function($output){
		if(!$output){
			error_log('Empty output');
		}else{
			if(gettype($output == 'object') || gettype($output == 'array')){
				error_log(print_r($output, 1));
			}else{
				error_log($output);
			}
		}

	}));

	return $twig;
}
add_filter('get_twig', 'add_to_twig');

function submit_private(){
	$response = new stdClass();
	$data = new stdClass();

	$response->errors = [];
	$response->status = 0;

	//error_log(print_r($_POST['data'], 1));

	if(!empty($_POST['data']['first-name']) &&
		!empty($_POST['data']['last-name']) &&
		!empty($_POST['data']['email-address']) &&
		!empty($_POST['data']['phone-number']) &&
		!empty($_POST['data']['tour-date']) &&
		!empty($_POST['data']['hotel']) &&
		!empty($_POST['data']['guests-num'])
	){
		$data->tour_name = sanitize_text_field($_POST['data']['tour-name']);
		$data->fname = sanitize_text_field($_POST['data']['first-name']);
		$data->lname = sanitize_text_field($_POST['data']['last-name']);
		$data->email = sanitize_email($_POST['data']['email-address']);
		$data->phone = sanitize_text_field($_POST['data']['phone-number']);
		$data->tour_date = sanitize_text_field($_POST['data']['tour-date']);
		$data->hotel = sanitize_text_field($_POST['data']['hotel']);
		$data->guest_num = sanitize_text_field($_POST['data']['guests-num']);

		$data->room_num = !empty($_POST['data']['room-num']) ? sanitize_text_field($_POST['data']['room-num']) : '';
		$data->photographer = !empty($_POST['data']['photographer']) ? sanitize_text_field($_POST['data']['photographer']) : '';
		$data->special_needs = !empty($_POST['data']['special-needs']) ? esc_textarea($_POST['data']['special-needs']) : '';
		$data->species = !empty($_POST['data']['species']) ? esc_textarea($_POST['data']['species']) : '';

		$to = get_theme_mod('email');
		$subject = 'New Private Tour Request';
		$body = 'New private tour request from ' . $data->fname . ' ' . $data->lname . "\r\n\r\n" .
			'Tour: ' . $data->tour_name . "\r\n" .
			'Name: ' . $data->fname . ' ' . $data->lname . "\r\n" .
			'Email: ' . $data->email . "\r\n" .
			'Phone: ' . $data->phone . "\r\n" .
			'Date: ' . $data->tour_date . "\r\n" .
			'Number of guests: ' . $data->guest_num . "\r\n" .
			'Hotel/Pickup location: ' . $data->hotel . "\r\n" .
			'Room Number: ' . $data->room_num . "\r\n" .
			'Photographer: ' . ($data->photographer == 'on' ? 'yes' : 'no') . "\r\n" .
			'Special Needs: ' . $data->special_needs . "\r\n" .
			'Target Species: ' . $data->species . "\r\n";
		$headers[] = 'From: ' . $data->fname . ' ' . $data->lname . '<' . $data->email . '>';
		$headers[] = 'Content-Type: text/plain; charset=UTF-8';

		if(wp_mail($to, $subject, $body, $headers)){
			$response->status = 1;
			$response->data = $data;
		}else{
			$response->status = -2;
			$response->errors[] = _e('There was an error sending your email', 'mvnp_basic_admin');
		}
	}else{
		$response->status = -1;
		$response->errors[] = _e('Oops! Looks like you missed something!', 'mvnp_basic_admin');
	}

	header('Content-Type:application/json;');
	echo json_encode($response);
	wp_die();
}
add_action('wp_ajax_nopriv_submit_private', 'submit_private');
