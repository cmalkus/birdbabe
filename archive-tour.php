<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

global $wp;
$island = $_GET['island'] ? $_GET['island'] : '';
$type = $_GET['type'] ? $_GET['type'] : '';

$args = array(
	'post_type' => 'tour',
	'posts_per_page' => -1,
	'orderby' => '',
	'order' => 'ASC',
);

$args['tax_query'] = array();

if($island != ''){
	array_push($args['tax_query'], array(
		'taxonomy' => 'tour-category',
		'field' => 'slug',
		'terms' => $island,
	));
}

if($type != ''){
	array_push($args['tax_query'], array(
		'taxonomy' => 'tour-type',
		'field' => 'slug',
		'terms' => $type,
	));
}

$context = Timber::context();
$context['post'] = new stdClass();
$context['post']->post_content = get_theme_mod('tours_copy');
if(isset($island) && !empty($island)){
	$context['title'] = sprintf(__('Tours in %s', 'mvnp_basic'), get_term_by('slug', $island, 'tour-category')->name);
}else{
	$context['title'] = __('Tours', 'mvnp_basic');
}

$context['post']->title = $context['title'];
$context['post']->link = home_url(add_query_arg(array(), $wp->request));
$context['post']->type = 'page';
$context['tour_islands'] = Timber::get_terms(array('taxonomy' =>'tour-category', 'order' => 'DESC'));
$context['tour_types'] = Timber::get_terms(array('taxonomy' =>'tour-type', 'order' => 'DESC'));
$context['tours'] = new Timber\PostQuery($args);

$context['sidebar'] = Timber::get_sidebar('tours-sidebar.php', $context);
Timber::render('archive-tour.twig', $context);
