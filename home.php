<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

global $wp;
$category = $_GET['category'] ? $_GET['category'] : '';

$args = array(
	'post_type' => 'post',
	'posts_per_page' => -1,
	'orderby' => '',
	'order' => 'DESC',
);

if($category != ''){
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => $category,
		),
	);
}

$context = Timber::context();

$context['categories'] = Timber::get_terms(array('taxonomy' =>'category', 'order' => 'DESC'));

$context['sidebar'] = Timber::get_sidebar('posts-sidebar.php', $context);
$context['posts'] = new Timber\PostQuery($args);
$context['cat_query'] = $category ? true : false;
$context['title'] = __('Mandy\'s Blog', 'mvnp_basic');
Timber::render('home.twig', $context);
