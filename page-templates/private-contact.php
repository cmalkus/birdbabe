<?php
/**
* Template Name: Private contact template
*
* Description:
*/

$context = Timber::context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( 'private-contact.twig', $context );
