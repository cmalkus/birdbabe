<?php
/**
 * The Template for displaying all single posts
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

//$data['tour_categories'] = Timber::get_terms();
$data['cta_widget'] = Timber::get_widgets('sidebar-cta');
Timber::render(array('single-tour-sidebar.twig'), $data);
