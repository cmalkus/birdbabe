<?php

Members::init();

Class Members{
	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_action('user_register', array(__CLASS__, 'create_member_page'));
		add_action('personal_options_update', array(__CLASS__, 'create_member_page'));
		add_action('edit_user_profile_update', array(__CLASS__, 'create_member_page'));
		add_action('delete_user', array(__CLASS__, 'delete_member_page'));
	}

	/**
	 * Create the user page post type. This is completely hidden from the admin. Right now it can be searched, but we'll probably turn that off.
	 */
	function create_post_type(){
		register_post_type('member',
			array(
				'label' => __('Users', 'mvnp_basic_admin'),
				'public' => true,
				'exclude_from_search' => true,
				'show_ui' => false,
				'show_in_nav_menus' => false,
				'show_in_menu' => false,
				'show_in_admin_bar' => false,
				'hierarchical' => false,
				'has_archive' => true,
				'publicly_queryable' => true,
				'rewrite' => array('slug' => 'user'),
				'query_var' => 'member',
			)
		);
	}

	/**
	 * Hook into user update and creation to make a private, personal post for each user. Admin can access.
	 * Check the DB to see if that page has been created, set the page author to that person (that how we make sure only they can access)
	 * @param  string $user_id The id of the user that the page is being created for
	 * @return null
	 */
	function create_member_page($user_id = ''){
		global $wpdb;
		$user = new WP_User($user_id);

		if(!$user->ID){
			return '';
		}

		// check if the user whose profile is updating has already a post
		$member_post_exists = $wpdb->get_var($wpdb->prepare(
			"SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type = 'member' and post_status = 'publish'", $user->data->user_nicename
		));

		$user_info = array_map(
			function($a){
				return $a[0];
			},
			get_user_meta($user->ID)
		);

		$post = array(
			'post_title' => $user->data->display_name,
			'post_name' => $user->data->user_nicename,
			'post_status' => 'publish',
			'post_type' => 'member',
			'post_author' => $user->ID,
		);

		if(!file_exists(get_user_dir($user)['path'])){
			wp_mkdir_p(get_user_dir($user)['path']);
		}

		if($member_post_exists){
			$post['ID'] = $member_post_exists;
			wp_update_post($post);
		} else {
			wp_insert_post($post);
		}
	}

	/**
	 * Deletes the user's page when the user is removed.
	 * @param int $user_id the user's id
	 */
	function delete_member_page($user_id, $reassign_id = NULL){
		require_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php';
		require_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php';

		global $wpdb;
		$dir = new WP_Filesystem_Direct(null);
		$user = new WP_User($user_id);

		if($reassign_id){
			$assignee = new WP_User($reassign_id);
		}

		$post_id = url_to_postid(static::member_permalink($user_id));
		wp_delete_post($post_id, true);

		if(file_exists(get_user_dir($user)['path'])){
			$dir->rmdir(get_user_dir($user)['path'], true);
		}
	}

	/**
	 * A QOL function to get the permalink of user pages based off user id or user nicename
	 * @param  string $user The user the link is being made for
	 * @return fn       The permalink for the page.
	 */
	public function member_permalink($user = ''){
		global $wpdb;

		if(!empty($user)){
			if(is_numeric($user)){ // user id
				$userObj = get_user_by('ID', $user);
			} else { // user nicename
				$userObj = -1;
			}
		} else {
			$userObj = wp_get_current_user();
			$name = isset($userObj->user_nicename) ? $userObj->user_nicename : '';
		}

		if(!isset($name)){
			$name = $userObj == -1 ? $user : $userObj->user_nicename;
		}

		$id = $wpdb->get_var($wpdb->prepare(
			"SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type = 'member' AND post_status = 'publish'", $name
		));

		return $id ? get_permalink($id) : '';
	}
}
