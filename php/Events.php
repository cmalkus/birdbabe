<?php

require_once get_template_directory() . '/php/vendor/autoload.php';

Events::init();

Class Events{
	const APPLICATION_NAME = 'MVNP Events Manager';
	const CREDENTIALS_PATH = TEMPLATEPATH . '/php/vendor/calendar-php-quickstart.json';
	const CLIENT_SECRET_PATH = TEMPLATEPATH . '/php/vendor/client_secret.json';
	const SCOPES = Google_Service_Calendar::CALENDAR;

	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_action('init', array(__CLASS__, 'create_taxonomy'));
		add_filter('manage_event_posts_columns', array(__CLASS__, 'event_table_head'));
		add_action('manage_event_posts_custom_column', array(__CLASS__, 'event_table_content'), 10, 2);
		add_filter('manage_edit-event_sortable_columns', array(__CLASS__, 'event_table_sorting'));
		add_filter('request', array(__CLASS__, 'start_date_column_orderby'));
		add_action('pre_get_posts', array(__CLASS__, 'auto_start_date_column_orderby'));
		add_action('restrict_manage_posts', array(__CLASS__, 'event_table_filtering'));
		add_filter('parse_query', array(__CLASS__, 'event_table_filter'));
		add_action('manage_posts_extra_tablenav', array(__CLASS__, 'g_cal_import_button'));
		add_filter('views_edit-event', array(__CLASS__, 'events_copy_textarea'));
		add_action('wp_ajax_import_google_cal', array(__CLASS__, 'import_google_cal'));
		add_action('wp_ajax_event_info_save', array(__CLASS__, 'event_info_meta_box_save'));
		add_action('save_post', array(__CLASS__, 'event_metaboxes_save'));
		add_action('add_meta_boxes', array(__CLASS__, 'add_meta_boxes'), 1);
	}

	/**
	 * Create the post type for the events.
	 */
	function create_post_type(){
		register_post_type('event',
			array(
				'labels' => array(
					'name' => __('Events', 'mvnp_basic_admin'),
					'singular_name' => __('Event', 'mvnp_basic_admin'),
					'menu_name' => __('Events', 'mvnp_basic_admin'),
				),
				'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
				'taxonomies' => array('event-category'),
				'menu_icon' => 'dashicons-calendar-alt',
				'public' => true,
				'has_archive' => true,
				'publicly_queriable' => true,
				'rewrite' => array('slug' => 'events'),
			)
		);
	}

	/**
	 * Create event category taxonomy
	 */
	function create_taxonomy(){
		register_taxonomy(
			'event-category',
			'event',
			array(
				'label' => __('Event Categories', 'mvnp_basic_admin'),
				'rewrite' => true,
				'hierarchical' => true,
				'show_in_nav_menus' => false,
			)
		);
	}

	function add_meta_boxes(){
		add_meta_box('event-metadata', __('Event Details', 'mvnp_basic_admin'), array(__CLASS__, 'event_metaboxes'), 'event', 'normal', 'default');
	}

	/**
	 * Saves the values to theme mod on save
	 * @return null         null
	 */
	function event_info_meta_box_save(){
		if(!isset($_POST['data']['event_list_meta_box_nonce']) || !wp_verify_nonce($_POST['data']['event_list_meta_box_nonce'], 'event_list_meta_box_nonce')){
			return;
		}

		if(!current_user_can('activate_plugins') || !current_user_can('edit_theme_options')){
			return;
		}

		set_theme_mod('events_copy', urldecode($_POST['data']['events_copy']));
	}

	/**
	 * Adds the import calendar button on events page
	 * @param  string $screen the name of the current wp admin view
	 */
	function events_copy_textarea($screen){
		$screen = get_current_screen();
		$event_info = get_theme_mods();

		if($screen->post_type == 'event'){
			wp_nonce_field('event_list_meta_box_nonce', 'event_list_meta_box_nonce');
			echo '<div class="clearfix">';
			echo '<label id="events-copy">' . __('Events Copy', 'mvnp_basic_admin');
			echo '<textarea id="events_copy" class="widefat copy-input i18n-multilingual" name="events_copy" rows="5">' .  (esc_attr($event_info['events_copy']) != '' ? esc_attr($event_info['events_copy']) : '') . '</textarea>';
			echo '</label>';
			echo '</div>';
			echo '<button class="button button-primary save-event-copy" type="button" title="' . __('Save Event Copy', 'mvnp_basic_admin') . '">' . __('Save Event Copy', 'mvnp_basic_admin') . '</button><hr>';
		}
	}

	/**
	 * Adds the start date column
	 * @param array $defaults the default wp post table headers
	 * @return array the new table headers
	 */
	function event_table_head($defaults){
		$new = array();

		foreach($defaults as $key => $title){
			$new[$key] = $title;
			if($key == 'title'){
				$new['start_date'] = __('Start Date', 'mvnp_basic_admin');
			}
		}
		return $new;
	}

	/**
	 * Populates the start date column
	 * @param  string $column_name Name of the column
	 * @param  int $post_id     the post id
	 */
	function event_table_content($column_name, $post_id){
		if($column_name == 'start_date'){
			if(get_post_meta($post_id, 'recurrence', true) == ''){
				$start_date = get_post_meta($post_id, 'start_date', true);
				echo date('F j, Y', intval($start_date));
			} else {
				_e('Recurring Event', 'mvnp_basic_admin');
			}
		}
	}

	/**
	 * Makes the start date column sortable
	 * @param  array $columns The posts column headers
	 * @return array          The posts column headers
	 */
	function event_table_sorting($columns){
		$columns['start_date'] = 'start_date';
		return $columns;
	}

	/**
	 * Orderes the evenst by date when you click it in the menu
	 * @param  array $vars the url query vars
	 * @return array       the modified url query vars
	 */
	function start_date_column_orderby($vars){
		if(isset($vars['orderby']) && 'start_date' == $vars['orderby']){
			$vars = array_merge($vars, array(
				'meta_key' => 'start_date',
				'orderby' => 'meta_value',
			));
		}

		return $vars;
	}

	/**
	 * Orders the evenst by date by default.
	 * @param  array $query the url query vars
	 */
	function auto_start_date_column_orderby($query){
		global $pagenow;
		if(is_admin() && 'edit.php' == $pagenow && !isset($_GET['orderby']) && $_GET['post_type'] == 'event'){
			$query->set('meta_key', 'start_date');
			$query->set('orderby', 'meta_value');
			$query->set('order', 'ASC');
		}
	}

	/**
	 * Adds the droptdown filter
	 * @param  string $screen the name of the current wp admin view
	 */
	function event_table_filtering($screen){
		if($screen == 'event'){?>
			<select name="end_date">
				<option value="all" <?php if($_GET['end_date'] == 'all'): ?>selected="selected"<?php endif ?>><?php _e('All events', 'mvnp_basic_admin') ?></option>
				<option value="past" <?php if($_GET['end_date'] == 'past'): ?>selected="selected"<?php endif ?>><?php _e('Past events', 'mvnp_basic_admin') ?></option>
				<option value="upcoming" <?php if(!$_GET['end_date'] || $_GET['end_date'] == 'upcoming'): ?>selected="selected"<?php endif ?>><?php _e('Upcoming events', 'mvnp_basic_admin') ?></option>
			</select>
			<?php
		}
	}

	/**
	 * This is for the dropdown filter. It shows upcoming, past or all Events.
	 * @param array $query the url query vars
	 * TODO right now upcoming events shows all repeating events as well, even if they are done. Need to write in some logic to check if its over
	 */
	function event_table_filter($query){
		if(is_admin() AND $query->query['post_type'] == 'event'){
			$qv = &$query->query_vars;
			$qv['meta_query'] = array();
			$now = date('U', time());

			switch ($_GET['end_date']){
			case 'past':
				$qv['meta_query'][] = array(
					'key' => 'end_date',
					'value' => $now,
					'compare' => '<',
				);
				break;
			case 'upcoming':
				$qv['meta_query'][] = array(
					'relation' => 'OR',
					array(
						'key' => 'recurrence',
						'compare' => '!=',
						'value' => '',
					),
					array(
						'key' => 'end_date',
						'value' => $now,
						'compare' => '>=',
					),
				);
				break;
			case 'all':
				$qv['meta_query'][] = array();
				break;
			default:
				$qv['meta_query'][] = array(
					'relation' => 'OR',
					array(
						'key' => 'recurrence',
						'compare' => '!=',
						'value' => '',
					),
					array(
						'key' => 'end_date',
						'value' => $now,
						'compare' => '>=',
					),
				);
				break;
			}

			if(!empty($_GET['orderby']) AND $_GET['orderby'] == 'end_date'){
				$qv['orderby'] = 'meta_value';
				$qv['meta_key'] = 'end_date';
				$qv['order'] = strtoupper($_GET['order']);
			}
		}
	}

	/**
	 * Returns an authorized API client.
	 * @return Google_Client the authorized client object
	 */
	protected function get_google_client(){
		$client = new Google_Client();
		$client->setApplicationName(self::APPLICATION_NAME);
		$client->setScopes(self::SCOPES);
		$client->setAuthConfig(self::CLIENT_SECRET_PATH);
		$client->setAccessType('offline');
		$client->setPrompt('select_account consent');

		// Load previously authorized credentials from a file.
		$credentialsPath = self::CREDENTIALS_PATH;
		if(file_exists($credentialsPath)){
			$accessToken = json_decode(file_get_contents($credentialsPath), true);

			$client->setAccessToken($accessToken);

			// Refresh the token if it's expired.
			if($client->isAccessTokenExpired()){
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
				file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
			}
		}

		return $client;
	}

	/**
	 * Adds the import calendar button on events page
	 * @param  string $screen the name of the current wp admin view
	 */
	function g_cal_import_button($screen){
		$screen = get_current_screen();
		if($screen->post_type == 'event' && file_exists(get_template_directory() . '/php/vendor/calendar-php-quickstart.json') && file_exists(get_template_directory() . '/php/vendor/client_secret.json') && file_exists(get_template_directory() . '/php/vendor/autoload.php')){
			echo '<button class="button button-primary import-google-cal" type="button" title="Import Google Calendar">' . __('Import Google Calendar', 'mvnp_basic_admin') . '</button>';
		}
	}

	/**
	 * Grabs all the events from your google cal from today forward. Checks to see if that event exists in WP and either updates or inserts a new post with meta values.
	 */
	function import_google_cal(){
		global $wpdb;

		$google_calendar_client = static::get_google_client();
		$calendarId = 'primary';
		$optParams = array(
			'maxResults' => 50,
			'timeMin' => date('c'),
		);

		$google_calendar_service = new Google_Service_Calendar($google_calendar_client);

		$results = $google_calendar_service->events->listEvents($calendarId, $optParams);

		foreach($results->getItems() as $event){
			$event_post_exists = $wpdb->get_var($wpdb->prepare(
				"SELECT DISTINCT $wpdb->posts.ID FROM $wpdb->posts, $wpdb->postmeta
				WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND
				$wpdb->posts.post_type = 'event' AND
				$wpdb->postmeta.meta_key = 'google_id' AND
				meta_value = %s",
				$event->id
			));
			$post = array(
				'post_title' => $event->getSummary() ? $event->getSummary() : 'Untitled Event',
				'post_content' => $event->description ? $event->description : '',
				'post_status' => 'publish',
				'post_type' => 'event',
			);

			if($event_post_exists){
				$post['ID'] = $event_post_exists;
				$post_id = wp_update_post($post);
				update_post_meta($post_id, 'recurrence', $event->recurrence);
				update_post_meta($post_id, 'location', $event->location);

				if($event->start->dateTime && $event->end->dateTime){
					update_post_meta($post_id, 'start_date', strtotime($event->start->dateTime));
					update_post_meta($post_id, 'end_date', strtotime($event->end->dateTime));
					update_post_meta($post_id, 'all_day', false);
				} else {
					update_post_meta($post_id, 'start_date', strtotime($event->start->date));
					update_post_meta($post_id, 'end_date', strtotime($event->end->date));
					update_post_meta($post_id, 'all_day', true);
				}
			} else {
				$post_id = wp_insert_post($post);
				add_post_meta($post_id, 'google_id', $event->id, true);
				add_post_meta($post_id, 'recurrence', $event->recurrence, true);
				add_post_meta($post_id, 'location', $event->location, true);

				if($event->start->dateTime && $event->end->dateTime){
					add_post_meta($post_id, 'start_date', strtotime($event->start->dateTime), true);
					add_post_meta($post_id, 'end_date', strtotime($event->end->dateTime), true);
					add_post_meta($post_id, 'all_day', false, true);
				} else {
					add_post_meta($post_id, 'start_date', strtotime($event->start->date), true);
					add_post_meta($post_id, 'end_date', strtotime($event->end->date), true);
					add_post_meta($post_id, 'all_day', true, true);
				}
			}
		}
	}

	/**
	 * Makes all the events meta boxes.
	 * Recurring values are extracted from the ics string and put into an array. Boring docs here: https://tools.ietf.org/html/rfc5545
	 */
	function event_metaboxes(){
		global $post;

		if($error = get_transient('event_save_error')){
			mvnp_basic_notice($error, 'notice-error');
			delete_transient('event_save_error');
		}

		$location = get_post_meta($post->ID, 'location', true);
		$location_name = get_post_meta($post->ID, 'location_name', true);
		$start = get_post_meta($post->ID, 'start_date', true);
		$end = get_post_meta($post->ID, 'end_date', true);
		$all_day = get_post_meta($post->ID, 'all_day', true);
		$recurrence = explode('RRULE:', get_post_meta($post->ID, 'recurrence', true)[0])[1];
		$start_date = $start ? date('Y-m-d', $start) : '';
		$start_time = $start ? date('H:i', $start) : '';
		$end_date = $end ? date('Y-m-d', $end) : '';
		$end_time = $end ? date('H:i', $end) : '';

		parse_str(strtr($recurrence, ';', '&'), $recurrence);

		if(array_key_exists('BYDAY', $recurrence)){
			$recurrence['BYDAY'] = explode(',', $recurrence['BYDAY']);
		}

		wp_nonce_field('event_metabox_nonce', 'event_metabox_nonce');
		require_once get_template_directory() . '/php/partials/event-view.php';
	}

	/**
	 * Save event posts. Lots going on here, but it basically writes post meta and pushes values to the google calendar.
	 * Recurring values are extracted from the array and put into ics format. Boring docs here: https://tools.ietf.org/html/rfc5545
	 * @param  int $post_id ID of the post being saved.
	 */
	function event_metaboxes_save($post_id){
		if(!isset($_POST['event_metabox_nonce']) || !wp_verify_nonce($_POST['event_metabox_nonce'], 'event_metabox_nonce')){
			return;
		}

		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
			return;
		}

		if(!current_user_can('edit_post', $post_id)){
			return;
		}

		$google_id = get_post_meta($post_id, 'google_id', true) or 0;

		$location = $_POST['no_location'] == 'on' ? '' : sanitize_text_field($_POST['location']);
		$location_name = $_POST['no_location'] == 'on' ? '' : sanitize_text_field($_POST['location_name']);
		$start = strtotime($_POST['start_date'] . ' ' . $_POST['start_time']);
		$end = strtotime($_POST['end_date'] . ' ' . $_POST['end_time']);
		$all_day = $_POST['all_day'];

		$content = get_post($post_id)->post_content;
		$title = get_the_title($post_id);

		if($_POST['recurring'] == 'on' && (empty($_POST['freq']) || !isset($_POST['freq']))){
			set_transient('event_save_error', 'Frequency is required', 45);
			return false;
		} else {
			$recurrence['FREQ'] = $_POST['freq'];
		}

		if($recurrence['FREQ'] != 'DAILY' && $_POST['INTERVAL'] > 1){
			$recurrence['INTERVAL'] = $_POST['INTERVAL'];
		}

		if($recurrence['FREQ'] == 'WEEKLY'){
			$recurrence['BYDAY'] = array();

			foreach($_POST['recurr_days'] as $day){
				if(!empty($day)){
					$recurrence['BYDAY'][] = $day;
				} else {
					set_transient('event_save_error', __('Must select at least one day per week to recur.', 'mvnp_basic_admin'), 45);
					return false;
				}
			}
		}

		if($recurrence['FREQ'] == 'MONTHLY' && $_POST['recurr_by'] == 'by_day'){

			$recurrence['BYDAY'] = array();
			$date = strtotime($_POST['start_date']);
			$day = date('l', $date);
			$week = date('W', $date);

			$first_the_day_of_month = date('W', strtotime('first ' . $day . ' of ' . date('F Y', $date)));
			$nth = 1 + ($week < $first_the_day_of_month ? $week : $week - $first_the_day_of_month);

			switch ($day){
				case 'Sunday':
					$day = 'SU';
					break;
				case 'Monday':
					$day = 'MO';
					break;
				case 'Tuesday':
					$day = 'TU';
					break;
				case 'Wednesday':
					$day = 'WE';
					break;
				case 'Thursday':
					$day = 'TH';
					break;
				case 'Friday':
					$day = 'FR';
					break;
				case 'Saturday':
					$day = 'SA';
					break;
			}
			$recurrence['BYDAY'][] = $nth . $day;
		}

		if($_POST['ends'] == 'count_opt'){
			$recurrence['COUNT'] = $_POST['COUNT'];
		} elseif($_POST['ends'] == 'until_opt'){
			$recurrence['UNTIL'] = date('Ymd\\THis\\Z', strtotime($_POST['UNTIL']));
		}

		if($_POST['recurring'] == 'on' && count($recurrence) > 0){
			$recurrence_str = 'RRULE:';
			foreach($recurrence as $key => $value){
				if(gettype($value) == 'array'){
					$value = implode(',', $value);
				}
				$recurrence_str .= $key . '=' . $value . ';';
			}
		}

		$args = array(
			'summary' => $title,
			'location' => $location,
			'description' => $content,
			'start' => array(
				'dateTime' => date('c', $start),
				'timeZone' => get_option('timezone_string'),
			),
			'end' => array(
				'dateTime' => date('c', $end),
				'timeZone' => get_option('timezone_string'),
			),
		);

		if($_POST['recurring'] == 'on' && strlen($recurrence_str) > 0){
			$args['recurrence'] = array($recurrence_str);
			update_post_meta($post_id, 'recurrence', array($recurrence_str));
		} else {
			$args['recurrence'] = '';
			update_post_meta($post_id, 'recurrence', '');
		}

		if($all_day == 'on'){
			$args['start'] = array(
				'date' => date('Y-m-d', $start),
			);

			$args['end'] = array(
				'date' => date('Y-m-d', $end),
			);

			update_post_meta($post_id, 'all_day', true);
		} else {
			update_post_meta($post_id, 'all_day', false);
		}

		if(file_exists(get_template_directory() . '/php/vendor/calendar-php-quickstart.json') && file_exists(get_template_directory() . '/php/vendor/client_secret.json') && file_exists(get_template_directory() . '/php/vendor/autoload.php')){
			$google_calendar_client = static::get_google_client();
			$google_calendar_service = new Google_Service_Calendar($google_calendar_client);

			$event = new Google_Service_Calendar_Event($args);
			$exists = $google_calendar_service->events->get('primary', $google_id);

			if($exists->id){
				$event = $google_calendar_service->events->update('primary', $google_id, $event);
			} else {
				$event = $google_calendar_service->events->insert('primary', $event);
				add_post_meta($post_id, 'google_id', $event->id);
			}
		}

		update_post_meta($post_id, 'location', $location);
		update_post_meta($post_id, 'location_name', $location_name);
		update_post_meta($post_id, 'start_date', $start);
		update_post_meta($post_id, 'end_date', $end);
	}

	/**
	 * This big nasty thing is what expands repeating events into each component day.
	 * @param  array $args       Arguments for the WP_Query
	 * @param  int $date_start Unix timestamp of the start date of the range
	 * @param  int $date_end   Unix timestamp of the end date of the range
	 * @return array             The posts from the query, expanded to each day they run
	 */
	public function expand_recurring_events($args, $date_start, $date_end){
		global $post;
		$query = new WP_Query($args);
		$expanded_posts = array();

		while($query->have_posts()){
			$query->the_post();
			/*
			 * If the event starts and ends on the same day and there is no recurrence, put it in the array as is
			 */
			if(date('Ymd', $post->start_date) == date('Ymd', $post->end_date) && $post->recurrence == ''){
				$expanded_posts[] = $post;
				/*
				 * Otherwise, if it spans more than one day and does not recur, loop trough from the start to end, one day at a time and add them to the array
				 */
			} elseif(date('Ymd', $post->start_date) < date('Ymd', $post->end_date) && $post->recurrence == ''){
				while($post->start_date < $post->end_date){
					$expanded_posts[] = new WP_Post($post);
					$post->start_date = strtotime('midnight', strtotime('+1 day', $post->start_date));
				}
				/*
				 * Then if it does recur, format our rrules into an array
				 */
			} elseif(is_array($post->recurrence) && strlen($post->recurrence[0]) > 0){
				$recurrence = explode('RRULE:', $post->recurrence[0])[1];
				parse_str(strtr($recurrence, ';', '&'), $recurrence);

				/*
				 * Rruel contains an interval if its more than one unit. If not, we need to set it to one
				 */
				$interval = array_key_exists('INTERVAL', $recurrence) ? $recurrence['INTERVAL'] : '1';

				/*
				 * Need to change the frequency to singular form for adding to date objects
				 */
				switch ($recurrence['FREQ']){
				case 'DAILY':
					$freq = 'day';
					break;
				case 'WEEKLY':
					$freq = 'week';
					break;
				case 'MONTHLY':
					$freq = 'month';
					break;
				case 'YEARLY':
					$freq = 'year';
					break;
				}

				/*
				 * If it recurs on specific days of the week (monthly or weekly) we need to format those days into a php date parsable format.
				 * If it is by month, it will only have one BYDAY, which also contains a number. need to split that up.
				 */
				if(array_key_exists('BYDAY', $recurrence)){
					$recurrence['BYDAY'] = explode(',', $recurrence['BYDAY']);

					if($freq == 'month' && count($recurrence['BYDAY'] == 1)){
						$recurrence['BYDAY'] = preg_split('#(?<=\d)(?=[a-z])#i', $recurrence['BYDAY'][0]);
					}

					foreach($recurrence['BYDAY'] as $key => $val){
						switch ($val){
						case 'SU':
							$recurrence['BYDAY'][$key] = 'Sunday';
							break;
						case 'MO':
							$recurrence['BYDAY'][$key] = 'Monday';
							break;
						case 'TU':
							$recurrence['BYDAY'][$key] = 'Tuesday';
							break;
						case 'WE':
							$recurrence['BYDAY'][$key] = 'Wednesday';
							break;
						case 'TH':
							$recurrence['BYDAY'][$key] = 'Thursday';
							break;
						case 'FR':
							$recurrence['BYDAY'][$key] = 'Friday';
							break;
						case 'SA':
							$recurrence['BYDAY'][$key] = 'Saturday';
							break;
						}
					}
				}

				/*
				 * Events can recur a forever, a specific number of times, or until a certain date. Until a certain date is here first.
				 *
				 *
				 */
				if(array_key_exists('UNTIL', $recurrence)){
					$until = date(strtotime($recurrence['UNTIL']) - get_option('gmt_offset') * 3600);
					if(array_key_exists('BYDAY', $recurrence)){
						if($freq == 'week'){
							while($post->start_date < $until && $post->start_date < $date_end){
								if($post->start_date >= $date_start && in_array(date('l', $post->start_date), $recurrence['BYDAY'])){
									$expanded_posts[] = new WP_Post($post);
								}
								if(date('N', $post->start_date) == 7){
									$post->start_date = date(strtotime('+1 day +' . ($interval - 1) . ' ' . $freq, $post->start_date));
									$post->end_date = date(strtotime('+1 day +' . ($interval - 1) . ' ' . $freq, $post->end_date));
								} else {
									$post->start_date = date(strtotime('+1 day', $post->start_date));
									$post->end_date = date(strtotime('+1 day', $post->end_date));
								}
							}
						} elseif($freq == 'month'){
							if($date_end < $post->start_date || $date_start > $until){
								break;
							}
							$post->start_date = date(strtotime('+' . ($recurrence['BYDAY'][0] - 1) . ' week', $date_start));
							$post->end_date = date(strtotime('+' . ($recurrence['BYDAY'][0] - 1) . ' week', $date_start . '+' . ((strtotime($post->end_date) - strtotime($post->start_date)) / 60 / 60 / 24) . ' days'));

							while($post->start_date < $until && $post->start_date < $date_end){
								if($post->start_date >= $date_start && in_array(date('l', $post->start_date), $recurrence['BYDAY'])){
									$expanded_posts[] = new WP_Post($post);
									break;
								}
								$post->start_date = date(strtotime('+1 day', $post->start_date));
								$post->end_date = date(strtotime('+1 day', $post->end_date));
							}
						}
					} else {
						while($post->start_date < $until && $post->start_date < $date_end){
							if($post->start_date >= $date_start){
								$expanded_posts[] = new WP_Post($post);
							}
							$post->start_date = date(strtotime('+' . $interval . ' ' . $freq, $post->start_date));
							$post->end_date = date(strtotime('+' . $interval . ' ' . $freq, $post->end_date));
						}
					}
				} elseif(array_key_exists('COUNT', $recurrence)){
					if(array_key_exists('BYDAY', $recurrence)){
						if($freq == 'week'){
							for($i = 0; $i < $recurrence['COUNT'] && $post->start_date < $date_end; $i++){
								if($post->start_date >= $date_start && in_array(date('l', $post->start_date), $recurrence['BYDAY'])){
									$expanded_posts[] = new WP_Post($post);
								}
								if(date('N', $post->start_date) == 7){
									$post->start_date = date(strtotime('+1 day +' . ($interval - 1) . ' ' . $freq, $post->start_date));
									$post->end_date = date(strtotime('+1 day +' . ($interval - 1) . ' ' . $freq, $post->end_date));
								} else {
									$post->start_date = date(strtotime('+1 day', $post->start_date));
									$post->end_date = date(strtotime('+1 day', $post->end_date));
								}
							}
						} elseif($freq == 'month'){
							if($date_end < $post->start_date){
								break;
							}
							$post->start_date = date(strtotime('+' . ($recurrence['BYDAY'][0] - 1) . ' week', $post->start_date));
							$post->end_date = date(strtotime('+' . ($recurrence['BYDAY'][0] - 1) . ' week', $date_start . '+' . ((strtotime($post->end_date) - strtotime($post->start_date)) / 60 / 60 / 24) . ' days'));

							for($i = 0; $i < $recurrence['COUNT'] && $post->start_date < $date_end; $i++){
								if($post->start_date >= $date_start && in_array(date('l', $post->start_date), $recurrence['BYDAY'])){
									$expanded_posts[] = new WP_Post($post);
									break;
								}
								$post->start_date = date(strtotime('+1 day', $post->start_date));
								$post->end_date = date(strtotime('+1 day', $post->end_date));
							}
						}
					} else {
						for($i = 0; $i < $recurrence['COUNT'] && $post->start_date < $date_end; $i++){
							if($post->start_date >= $date_start){
								$expanded_posts[] = new WP_Post($post);
							}
							$post->start_date = date(strtotime('+' . $interval . ' ' . $freq, $post->start_date));
							$post->end_date = date(strtotime('+' . $interval . ' ' . $freq, $post->end_date));
						}
					}
				} else {
					if(array_key_exists('BYDAY', $recurrence)){
						if($freq == 'week'){
							while($post->start_date < $date_end){
								if($post->start_date >= $date_start && in_array(date('l', $post->start_date), $recurrence['BYDAY'])){
									$expanded_posts[] = new WP_Post($post);
								}
								if(date('N', $post->start_date) == 7){
									$post->start_date = date(strtotime('+1 day +' . ($interval - 1) . ' ' . $freq, $post->start_date));
									$post->end_date = date(strtotime('+1 day +' . ($interval - 1) . ' ' . $freq, $post->end_date));
								} else {
									$post->start_date = date(strtotime('+1 day', $post->start_date));
									$post->end_date = date(strtotime('+1 day', $post->end_date));
								}
							}
						} elseif($freq == 'month'){
							if($date_end < $post->start_date){
								break;
							}
							$post->start_date = date(strtotime('+' . ($recurrence['BYDAY'][0] - 1) . ' week', $date_start));
							$post->end_date = date(strtotime('+' . ($recurrence['BYDAY'][0] - 1) . ' week', $date_start . '+' . ((strtotime($post->end_date) - strtotime($post->start_date)) / 60 / 60 / 24) . ' days'));

							while($post->start_date < $date_end){
								if($post->start_date >= $date_start && in_array(date('l', $post->start_date), $recurrence['BYDAY'])){
									$expanded_posts[] = new WP_Post($post);
									break;
								}
								$post->start_date = date(strtotime('+1 day', $post->start_date));
								$post->end_date = date(strtotime('+1 day', $post->end_date));
							}
						}
					} else {
						while($post->start_date < $date_end){
							if($post->start_date >= $date_start){
								$expanded_posts[] = new WP_Post($post);
							}
							$post->start_date = date(strtotime('+' . $interval . ' ' . $freq, $post->start_date));
							$post->end_date = date(strtotime('+' . $interval . ' ' . $freq, $post->end_date));
						}
					}
				}
			}
		}

		return $expanded_posts;
	}
}
