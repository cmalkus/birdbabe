<?php
/**
 * Load scripts and css specific to the admin
 */
function admin_load_files(){ ?>
	<script type="text/javascript">
		var templateUrl = '<?php echo get_stylesheet_directory_uri(); ?>',
			siteUrl = '<?php echo get_site_url(); ?>',
			postId = <?php global $post; echo (isset($post->ID)) ? $post->ID : 0; ?>,
			repeating, no_location;
	</script>
	<?php

	/**
	 * Load text domain for admin translations
	 */
	$mo_path = get_template_directory() . '/languages/';
	if(file_exists($mo_path . get_locale() . '_admin.mo')){
		load_textdomain('mvnp_basic_admin', $mo_path . get_locale() . '_admin.mo');
	}

	wp_enqueue_media();
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-draggable');
	wp_enqueue_script('google_maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyDUmWrI564wWhoAnJmync64ZZPOHAYe1Ac');
	wp_enqueue_script('admin_js', get_stylesheet_directory_uri() . '/js/app/admin.js');
	wp_enqueue_style('admin_css', get_stylesheet_directory_uri() . '/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_load_files');


/**
 * This adds custom fields to the theme customization. This is for social media, contact options, and the google analytics hook ID
 * @param  Object $wp_customize The WP customize object that we are ading to.
 */
function mvnp_basic_customize_register($wp_customize){
	$wp_customize->add_section('company_info', array(
		'title' => __('Company Information', 'mvnp_basic_admin'),
		'priority' => 30,
	));

	$wp_customize->add_setting('single_unit_info', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('all_unit_info', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('events_copy', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('tours_copy', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('logo', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_control', array(
		'label' => __('Company logo', 'mvnp_basic_admin'),
		'description' => __('This logo will only be used for your Google rich card. Site-wide logos will need to be changed by a developer.', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'logo',
	)));

	$wp_customize->add_setting('phone_number', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'phone_number_control', array(
		'label' => __('Phone Number', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'phone_number',
		'type' => 'tel',
	)));

	$wp_customize->add_setting('email', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'email_control', array(
		'label' => __('Email', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'email',
		'type' => 'email',
	)));

	$wp_customize->add_setting('street_address_1', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'street_address_1_control', array(
		'label' => __('Street Address 1', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'street_address_1',
	)));

	$wp_customize->add_setting('street_address_2', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'street_address_2_control', array(
		'label' => __('Street Address 2', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'street_address_2',
	)));

	$wp_customize->add_setting('city', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'city_control', array(
		'label' => __('City', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'city',
	)));

	$wp_customize->add_setting('state', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'state_control', array(
		'label' => __('State', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'state',
	)));

	$wp_customize->add_setting('postal', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'postal_control', array(
		'label' => __('Postal', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'postal',
		'type' => 'number',
	)));

	$wp_customize->add_setting('twitter_link', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'company_twitter_control', array(
		'label' => __('Twitter Link', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'twitter_link',
		'type' => 'url',
	)));

	$wp_customize->add_setting('facebook_link', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'company_facebook_control', array(
		'label' => __('FaceBook Link', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'facebook_link',
		'type' => 'url',
	)));

	$wp_customize->add_setting('instagram_link', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'company_instagram_control', array(
		'label' => __('Instagram Link', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'instagram_link',
		'type' => 'url',
	)));

	$wp_customize->add_setting('trip_advisor_link', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'company_trip_advisor_control', array(
		'label' => __('Trip Advisor Link', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'trip_advisor_link',
		'type' => 'url',
	)));

	$wp_customize->add_setting('google_analytics_code', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'google_analytics_code_control', array(
		'label' => __('Google Anylitics Code', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'google_analytics_code',
	)));

	$wp_customize->add_setting('google_tags_code', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'google_tags_code_control', array(
		'label' => __('Google Tags Code', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'google_tags_code',
	)));

	$wp_customize->add_setting('fb_pixel_code', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'fb_pixel_code_control', array(
		'label' => __('Facebook Pixel Code', 'mvnp_basic_admin'),
		'section' => 'company_info',
		'settings' => 'fb_pixel_code',
	)));
}
add_action('customize_register', 'mvnp_basic_customize_register');

/**
 * Filters page templates so child pages with parents that start with "parent", only show files that start with "child"
 * and top level pages show all but those
 */
function filter_templates($templates){
	global $post;

	if(!isset($post->post_parent) || empty($post->post_parent)){
		foreach($templates as $slug => $name){
			if(strpos(basename($slug), 'child-') === 0){
				unset($templates[$slug]);
			}
		}
	}else{
		$parent = get_page_template_slug($post->post_parent);

		if(isset($parent) && !empty($parent) && strpos(basename($parent), 'parent-') === 0){
			foreach($templates as $slug => $name){
				if(strpos(basename($slug), 'child-') === 0){
					continue;
				}else{
					unset($templates[$slug]);
				}
			}
		}else{
			foreach($templates as $slug => $name){
				if(strpos(basename($slug), 'child-') === 0 || strpos(basename($slug), 'parent-') === 0){
					unset($templates[$slug]);
				}
			}
		}
	}

	return $templates;
}
add_filter('theme_page_templates', 'filter_templates');

/**
 * ajax-bound function that basically just executes get_page_templates, but sets the post and parent based on front end selects
 * @return [null] dies
 */
function ajax_get_page_templates(){
	global $post;

	$post = get_post($_GET['data']['post_id']);
	$post->post_parent = $_GET['data']['post_parent'];
	$templates = get_page_templates($post);

	echo json_encode($templates);
	wp_die();
}
add_action('wp_ajax_get_page_templates', 'ajax_get_page_templates');
