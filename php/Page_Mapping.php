<?php

Page_Mapping::init();

Class Page_Mapping{
	public function __construct(){}

	public function init(){
		global $post;

		add_action('admin_init', array(__CLASS__, 'page_mapping_init'));
		add_filter('display_post_states', array(__CLASS__, 'filter_display_post_states'), 10, 2);
		add_filter('template_include', array(__CLASS__, 'template_override'));
		add_action('admin_footer', array(__CLASS__, 'remove_page_template_select'), 100);
		add_action('save_post', array(__CLASS__, 'contact_meta_box_save'));

		if(get_option('mvnp-contact-page-mapping') == $post->ID){
			add_action('add_meta_boxes', array(__CLASS__, 'add_meta_boxes'), 1);
		}
	}

	/**
	 * Sets up page mapping for "standard" pages with static templates
	 */
	function page_mapping_init(){
		register_setting(
			'reading',
			'mvnp-about-page-mapping',
			array(__CLASS__, 'about_page_mapping_sanitize')
		);

		register_setting(
			'reading',
			'mvnp-contact-page-mapping',
			array(__CLASS__, 'contact_page_mapping_sanitize')
		);

		add_settings_field(
			'mvnp-page-mapping',
			__('Page Mapping', 'mvnp_basic_admin'),
			array(__CLASS__, 'page_mapping_callback'),
			'reading'
		);
	}

	function add_meta_boxes(){
		add_meta_box('contact_meta', __('Contact Information', 'mvnp_basic_admin'), array(__CLASS__, 'display_contact_information'), 'page', 'side', 'default');
	}

	/**
	 * Just makes sure the values are valid
	 * @param  [int] $input  the value (page id) from the select
	 * @return [int]        if the value not a valid page or is already one of the other defaults
	 */
	function about_page_mapping_sanitize($input){
		if('publish' == get_post_status($input) && get_option('page_on_front') != $input && get_option('page_for_posts') != $input && get_option('mvnp-contact-page-mapping') != $input){
			return $input;
		}

		return 0;
	}

	function contact_page_mapping_sanitize($input){
		if('publish' == get_post_status($input) && get_option('page_on_front') != $input && get_option('page_for_posts') != $input && get_option('mvnp-about-page-mapping') != $input){
			return $input;
		}

		return 0;
	}

	/**
	 * The markup inserted into the "Reading" section of the settings
	 */
	function page_mapping_callback(){?>
		<fieldset>
			<legend class="screen-reader-text"><?php _e('Page Mapping', 'mvnp_basic_admin');?></legend>
			<ul id="page-mapping-list">
				<li>
					<label for="about_page_mapping">
						<?php echo __('About page', 'mvnp_basic_admin') . ': ';
							wp_dropdown_pages(array(
								'name' => 'mvnp-about-page-mapping',
								'id' => 'about_page_mapping',
								'selected' => get_option('mvnp-about-page-mapping', true),
								'option_none_value' => '0',
								'show_option_none' => '- ' . __('Select', 'mvnp_basic_admin') . ' -',
							)); ?>
					</label>
				</li>
				<li>
					<label for="contact_page_mapping">
						<?php echo __('Contact page', 'mvnp_basic_admin') . ': ';
							wp_dropdown_pages(array(
								'name' => 'mvnp-contact-page-mapping',
								'id' => 'contact_page_mapping',
								'selected' => get_option('mvnp-contact-page-mapping', true),
								'option_none_value' => '0',
								'show_option_none' => '- ' . __('Select', 'mvnp_basic_admin') . ' -',
							)); ?>
					</label>
				</li>
			</ul>
		</fieldset>
	<?php }

	/**
	 * Hook into page_states to add the content after the page name in the pages list
	 * @param  [array] $post_states array of post states. like "draft" or "posts page"
	 * @param  [stdObj] $post        the global $post
	 * @return [array]              our modified array
	 */
	function filter_display_post_states($post_states, $post){
		if('page' === get_option('show_on_front')){
			if(intval(get_option('mvnp-about-page-mapping')) === $post->ID){
				$post_states['mvnp-about-page-mapping'] = __('About Page', 'mvnp_basic_admin');
			}

			if(intval(get_option('mvnp-contact-page-mapping')) === $post->ID){
				$post_states['mvnp-contact-page-mapping'] = __('Contact Page', 'mvnp_basic_admin');
			}
		}

		return $post_states;
	}

	/**
	 * override the default template hierarchy for our new post "states"
	 * @param  [string] $template string of the location of the template
	 * @return [string]           string of the location of our predetermined template
	 */
	function template_override($template){
		global $post;

		if(get_option('mvnp-about-page-mapping') == $post->ID && $post->post_type == 'page' && !$post->page_template && !get_search_query()){
			return file_exists(locate_template('about-page.php')) ? locate_template('about-page.php') : $template;
		} elseif(get_option('mvnp-contact-page-mapping') == $post->ID && $post->post_type == 'page' && !$post->page_template && !get_search_query()){
			return file_exists(locate_template('contact-page.php')) ? locate_template('contact-page.php') : $template;
		}
		return $template;
	}

	/**
	 * Removes the template selector from pages with "states"
	 */
	function remove_page_template_select(){
		global $post;

		if(in_array($post->ID, array(get_option('mvnp-about-page-mapping'), get_option('mvnp-contact-page-mapping'), get_option('page_on_front')))){?>
			<script type="text/javascript">
				var elem = document.getElementById('page_template');

				if(elem){
					var sib = elem.previousElementSibling;

					elem = elem.parentNode.removeChild(elem);
					sib = sib.parentNode.removeChild(sib);
				}
			</script>
		<?php }
	}

	/**
	 * Markup for the contact info.
	 */
	function display_contact_information(){
		wp_nonce_field('contact_meta_box_nonce', 'contact_meta_box_nonce');
		$contact_info = get_theme_mods();
		require_once get_template_directory() . '/php/partials/contact-view.php';
	}

	/**
	 * Saves the values to theme mod on save
	 * @param  int $post_id the post ID
	 * @return null         null
	 */
	function contact_meta_box_save($post_id){
		if(!isset($_POST['contact_meta_box_nonce']) || !wp_verify_nonce($_POST['contact_meta_box_nonce'], 'contact_meta_box_nonce')){
			return;
		}

		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
			return;
		}

		if(!current_user_can('edit_post', $post_id) || !current_user_can('edit_theme_options')){
			return;
		}

		set_theme_mod('street_address_1', $_POST['street_1']);
		set_theme_mod('street_address_2', $_POST['street_2']);
		set_theme_mod('city', $_POST['city']);
		set_theme_mod('state', $_POST['state']);
		set_theme_mod('postal', $_POST['postal']);
		set_theme_mod('phone_number', $_POST['phone_number']);
		set_theme_mod('email', $_POST['email']);
	}
}
