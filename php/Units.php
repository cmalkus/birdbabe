<?php

namespace Units;

require_once get_template_directory() . '/php/Unit_List_Table.php';

Units::init();

Class Units{
	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_action('admin_menu', array(__CLASS__,  'add_units_menu'));
		add_action('wp_ajax_update_unit_entry', array(__CLASS__, 'update_unit_entry'));
		add_action('wp_ajax_save_units', array(__CLASS__, 'unit_info_meta_box_save'));
		add_action('template_redirect', array(__CLASS__, 'template_redirect' ));
	}

	/**
	 * Create the unit page post type. This is completely hidden from the admin and will not ever have an actualy unit post.
	 * The purpose of this is to create al the labels and archives and proper routes for the type.
	 * All unit data is stored in a custom table and the "post" is created on the fly when you enter what would be a unit route
	 */
	function create_post_type(){
		register_post_type('unit',
			array(
				'labels' => array(
					'name' => __('Units', 'mvnp_basic_admin'),
					'singular_name' => __('Unit', 'mvnp_basic_admin'),
				),
				'public' => true,
				'exclude_from_search' => false,
				'show_ui' => false,
				'show_in_nav_menus' => false,
				'show_in_menu' => false,
				'show_in_admin_bar' => false,
				'hierarchical' => false,
				'has_archive' => true,
				'publicly_queryable' => true,
				'rewrite' => array('slug' => 'units'),
				'query_var' => 'unit',
			)
		);
	}

	/**
	 * Updated the individual row entry when it loses focus
	 * @return null
	 */
	function update_unit_entry(){
		global $wpdb;

		if(!isset($_POST['data']['unit_info_meta_box_nonce']) || !wp_verify_nonce($_POST['data']['unit_info_meta_box_nonce'], 'unit_info_meta_box_nonce')){
			return;
		}

		if(!current_user_can('activate_plugins') || !current_user_can('edit_theme_options')){
			return;
		}

		$column = $_POST['data']['column'];
		$row_id = $_POST['data']['row_id'];

		switch($column){
			case 'available':
			case 'unitNumber':
			case 'affordable':
			case 'level':
			case 'livingSqFt':
			case 'lanaiSqFt':
			case 'parking':
			case 'bedrooms':
			case 'bathrooms':
			case 'price':
				$value = intval($_POST['data']['value']);
				break;

			case 'floorPlanFile':
			case 'panoramicViewFile':
			case 'isometricFile':
			case 'floorMapFile':
				$value = sanitize_file_name($_POST['data']['value']);
				break;

			case 'floorPlan':
			case 'viewPlaneDirection':
			default:
				$value = sanitize_text_field($_POST['data']['value']);
				break;
		}

		// $query = "UPDATE wp_building_units SET $column = $value WHERE id = $row_id";

		$wpdb->update('wp_building_units', array($column => $value), array('id' => $row_id));
	}

	/**
	 * Saves the values to theme mod on save
	 * @return null         null
	 */
	function unit_info_meta_box_save(){
		if(!isset($_POST['data']['unit_info_meta_box_nonce']) || !wp_verify_nonce($_POST['data']['unit_info_meta_box_nonce'], 'unit_info_meta_box_nonce')){
			return;
		}

		if(!current_user_can('activate_plugins') || !current_user_can('edit_theme_options')){
			return;
		}

		set_theme_mod('all_unit_info', urldecode($_POST['data']['all_unit_info']));
		set_theme_mod('single_unit_info', urldecode($_POST['data']['single_unit_info']));
	}

	/**
	 * Adds the table view for units to the admin.
	 * @return bool just bail out if we dont have our table view class
	 */
	function add_units_menu(){
		add_menu_page('Units', 'Units', 'activate_plugins', 'units_page', array(__CLASS__, 'render_page'), 'dashicons-building', 25);
	}

	/**
	 * Renders the units table
	 * @return bool just bail out if we dont have our table view class
	 */
	function render_page(){
		$unit_info = get_theme_mods();

		$unitsTable = new Unit_List_Table();
		$unitsTable->prepare_items();

		?>
		<div class="wrap">
			<h1 id="units-title"><?php _e('Units', 'mvnp_basic_admin'); ?></h1>
			<div id="units-filter">
				<div class="clearfix">
					<label>
						<?php _e('All Units Copy', 'mvnp_basic_admin');?>
						<textarea id="all_unit_info" class="widefat copy-input i18n-multilingual" name="all_unit_info" rows="5"><?php if($unit_info['all_unit_info'] != ''){echo esc_attr($unit_info['all_unit_info']);}?></textarea>
					</label>
					<label>
						<?php _e('Single Unit Copy', 'mvnp_basic_admin');?>
						<textarea id="single_unit_info" class="widefat copy-input i18n-multilingual-curly" name="single_unit_info" rows="5"><?php if($unit_info['single_unit_info'] != ''){echo esc_attr($unit_info['single_unit_info']);}?></textarea>
					</label>
				</div>
				<button class="button button-primary save-unit-info" type="button" title="Save Unit Info"> <?php _e('Save Unit Info', 'mvnp_basic_admin'); ?> </button>
				<hr>
				<form id="unit-search" method="get">
					<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>">
					<input id="s" type="number" name="s" placeholder="<?php _e('Unit Number', 'mvnp_basic_admin'); ?>">
					<button class="button button-primary" type="submit" title="Search Unit Number"> <?php _e('Search Unit Number', 'mvnp_basic_admin'); ?> </button>
				</form>
				<form method="get">
					<?php wp_nonce_field('unit_info_meta_box_nonce', 'unit_info_meta_box_nonce'); ?>
					<?php $unitsTable->display_rows() ?>
				</form>
			</div>
		</div>
		<?php
	}

	/**
	 * Parse the unit description content and replace braketed strings with the appropriate numbers/strings
	 * @param  string   $text    the content to be parsed
	 * @param  stdClass $options an object containing the replacements
	 * @return string            the new content
	 */

	public function parse_unit_content(string $text, \stdClass $options){
		$re = '/\[\[([a-zA-Z_]+)\]\]/';
		preg_match_all($re, $text, $matches);

		foreach($matches[0] as $match){
			$trimmed = preg_replace($re, '${1}', $match);

			switch($trimmed){
				case 'affordable':
					$replacement = $options->$trimmed == 1 ? __('affordably priced', 'mvnp_basic') : __('market value', 'mvnp_basic');
					break;
				case 'level':
					$formatter = new \NumberFormatter(get_locale(), \NumberFormatter::ORDINAL);
					$replacement = $formatter->format($options->$trimmed);
					break;
				case 'total_sqft';
					$replacement = $options->livingSqFt + $options->lanaiSqFt;
					break;
				case 'price':
					$replacement = '$' . number_format($options->$trimmed, 0);
					break;
				case 'available':
					$replacement = $options->$trimmed == 1 ? __('This unit is still available', 'mvnp_basic') : __('This unit is no longer available', 'mvnp_basic');
					break;
				default:
					$replacement = $options->$trimmed;
					break;
			}
			$text = str_replace($match, $replacement, $text);
		}

		return $text;
	}

	/**
	 * since the units have no actual post associated with them, we have to make a fake page
	 * this just says if our uri is units, stop being 404 and render our template
	 * @return null
	 */
	function template_redirect( ){
		if(preg_match('/\/units\/[0-9]+/', $_SERVER['REQUEST_URI'])){
			global $wp_query, $post;
			preg_match('/(\d+)(?!.*\d)/', $_SERVER['REQUEST_URI'], $unit_number);

			$post = new \stdClass();
			$post->ID = -99;
			$post->unit_number = $unit_number[0];
			$post->post_title = sprintf(__('Unit %s', 'mvnp_basic'), $unit_number[0]);
			$post->post_status = 'publish';
			$post->comment_status = 'closed';
			$post->ping_status = 'closed';
			$post->post_name = sprintf(__('Unit %s', 'mvnp_basic'), $unit_number[0]);
			$post->post_type = 'unit';
			$post->filter = 'raw';
			$post = new \WP_Post($post);

			$wp_query->is_404 = false;
			$wp_query->is_single = true;
			$wp_query->query_vars['post_type'] = 'unit';
			$wp_query->posts = array($post);
			$wp_query->queried_object = $post;
			$wp_query->queried_object_id = $post->ID;
			$wp_query->found_posts = 1;
			$wp_query->post_count = 1;
			$wp_query->max_num_pages = 1;

			status_header(200);
			require_once get_template_directory() . '/single-unit.php';

			exit();
		}
	}
}
