<?php

Image_Utils::init();

Class Image_Utils{
	public function __construct(){}

	public function init(){
		add_filter('image_resize_dimensions', array(__CLASS__, 'mvnp_basic_thumbnail_upscale'), 10, 6);
		add_filter('get_image_tag', array(__CLASS__, 'image_add_lazy_load'), 10, 4);
		add_action('wp_generate_attachment_metadata', array(__CLASS__, 'resample_image'));
		add_action('delete_attachment', array(__CLASS__, 'delete_webp'));

		/**
		 * This turns off the srcsets in the wp visual editor. I wanted to use lazyloading and this was complicating the issue.
		 * once i figure out a good way to lazyload and use srcsets, this can be turned back on.
		 * TODO figure out lazyloading srcsets
		 */
		add_filter( 'wp_calculate_image_srcset', '__return_false' );
	}

	/**
	 * This will allow wordpress to scale images up. It's not ideal to scale small images up, but its better than having malformed images in the content
	 * @param  [type] $default [description]
	 * @param  Number $orig_w  Image's original width
	 * @param  Number $orig_h  Image's original height
	 * @param  Number $new_w   Width value for upscale
	 * @param  Number $new_h   Height value for upscale
	 * @param  Boolean $crop   Whether cropping of the image is allowed. If not, this function exits
	 * @return null|Array      Passes the new values to the WP image scaler
	 */
	function mvnp_basic_thumbnail_upscale($default, $orig_w, $orig_h, $new_w, $new_h, $crop){
		if(!$crop){
			return null;
		}

		$aspect_ratio = $orig_w / $orig_h;
		$size_ratio = max($new_w / $orig_w, $new_h / $orig_h);
		$crop_w = round($new_w / $size_ratio);
		$crop_h = round($new_h / $size_ratio);
		$s_x = floor(($orig_w - $crop_w) / 2);
		$s_y = floor(($orig_h - $crop_h) / 2);

		return array(0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h);
	}

	/**
	 * This will add "data-src" attruibute to images included in the wp visual editor. It takes the images, replaces the src with the
	 * lazy load image size, configurable below, and moves the original size into the data-src.
	 * @param String $html  the entire img tag
	 * @param Int $id    ID of the image attachment
	 */
	function image_add_lazy_load($html, $id){
		$image_meta = wp_get_attachment_metadata($id);

		$src = (string) reset(simplexml_import_dom(DOMDocument::loadHTML($html))->xpath("//img/@src"));
		$html = str_replace($src, wp_get_attachment_image_src($id, 'lazy_orginal_ratio')[0], $html);
		$html = str_replace('/>', 'data-src="' . $src . '" />', $html);

		//this adds webp when the image is inserted into the post. thats not really a good idea considering chrome is the only thing that supports webp.
		//the webp url should be parsed out of the source when the content is being prepared

		// if(file_exists(str_ireplace(array('.jpg', '.jpeg', '.png', '.gif'), '.webp', wp_upload_dir()['basedir'] . '/' . $image_meta['file']))){
		// 	$html = str_ireplace(array('.jpg', '.jpeg', '.png', '.gif'), '.webp', $html);
		// }

		return $html;
	}

	/**
	 * Use pngquant and jpeg-recompress libraries to reduce image sizes by 50%-90%. The function operates
	 * directly on the image files and leaves the meta values unchanged.
	 * @param  Obj $image_meta The uploaded file's meta
	 * @return Obj             The uploaded file's meta. These values have not been updated.
	 */
	function resample_image($image_meta){
		$original = $file_path = wp_upload_dir()['basedir'] . '/' . $image_meta['file'];
		list($image_width, $image_height, $mime_type, $image_attr) = getimagesize($file_path);

		switch ($mime_type){
			case 1: //gif
				if(file_exists(get_template_directory() . '/bin/gif2webp')){
					$webp_file = system(get_template_directory() . '/bin/gif2webp ' . $file_path . ' -o ' . str_ireplace('.gif', '.webp', $file_path));
				}
				break;
			case 2: //jpg
				if(file_exists(get_template_directory() . '/bin/jpeg-recompress') && class_exists('Imagick')){
					$file = new \Imagick(realpath($file_path));

					if($file->getImageColorspace() == \Imagick::COLORSPACE_CMYK){
						$file->transformImageColorspace(\Imagick::COLORSPACE_SRGB);

						if(file_exists(get_template_directory() . '/bin/sRGB_v4_ICC_preference.icc')){
							$icc_rgb = file_get_contents(get_template_directory() . '/bin/sRGB_v4_ICC_preference.icc');
							$file->profileImage('icc', $icc_rgb);
						}

						$file->setImageFormat('jpeg');
						file_put_contents($file_path, $file);
						$file->destroy();
					}

					$new_file = system(get_template_directory() . '/bin/jpeg-recompress -m smallfry ' . $file_path . ' ' . $file_path);
				}

				if(file_exists(get_template_directory() . '/bin/cwebp')){
					$webp_file = system(get_template_directory() . '/bin/cwebp ' . $file_path . ' -o ' . str_ireplace(array('.jpg', '.jpeg'), '.webp', $file_path));
				}
				break;
			case 3: //png
				if(file_exists(get_template_directory() . '/bin/pngquant')){
					$new_file = system(get_template_directory() . '/bin/pngquant --ext=.png --force ' . $file_path);
				}

				if(file_exists(get_template_directory() . '/bin/cwebp')){
					$webp_file = system(get_template_directory() . '/bin/cwebp ' . $file_path . ' -o ' . str_ireplace('.png', '.webp', $file_path));
				}
				break;
		}

		foreach($image_meta['sizes'] as $size => $val){
			preg_match('/.*\//', $image_meta['file'], $date_dir, PREG_OFFSET_CAPTURE);
			$file_path = wp_upload_dir()['basedir'] . '/' . $date_dir[0][0] . $val['file'];
			list($image_width, $image_height, $mime_type, $image_attr) = getimagesize($file_path);

			switch ($mime_type){
				case 1: //gif
					if(class_exists('Imagick')){
						$file = new \Imagick(realpath($original));
						$file = $file->coalesceImages();

						foreach($file as $frame){
							$frame->resizeImage($image_width, $image_height, Imagick::FILTER_LANCZOS, 0);
						}

						$file = $file->deconstructImages();
						$file->writeImages($file_path, true);

						if(file_exists(get_template_directory() . '/bin/gif2webp')){
							$webp_file = system(get_template_directory() . '/bin/gif2webp ' . $file_path . ' -o ' . str_ireplace('.gif', '.webp', $file_path));
						}

						$file->destroy();
					}
					break;
				case 2: //jpg
					if(file_exists(get_template_directory() . '/bin/jpeg-recompress') && class_exists('Imagick')){
						$file = new \Imagick(realpath($file_path));

						if($file->getImageColorspace() == \Imagick::COLORSPACE_CMYK){
							$file->transformImageColorspace(\Imagick::COLORSPACE_SRGB);

							if(file_exists(get_template_directory() . '/bin/sRGB_v4_ICC_preference.icc')){
								$icc_rgb = file_get_contents(get_template_directory() . '/bin/sRGB_v4_ICC_preference.icc');
								$file->profileImage('icc', $icc_rgb);
							}

							$file->setImageFormat('jpeg');
							file_put_contents($file_path, $file);
							$file->destroy();
						}

						$new_file = system(get_template_directory() . '/bin/jpeg-recompress -m ms-ssim ' . $file_path . ' ' . $file_path);
					}

					if(file_exists(get_template_directory() . '/bin/cwebp')){
						$webp_file = system(get_template_directory() . '/bin/cwebp ' . $file_path . ' -o ' . str_ireplace(array('.jpg', '.jpeg'), '.webp', $file_path));
					}
					break;
				case 3: //png
					if(file_exists(get_template_directory() . '/bin/pngquant')){
						$new_file = system(get_template_directory() . '/bin/pngquant --ext=.png --force ' . $file_path);
					}

					if(file_exists(get_template_directory() . '/bin/cwebp')){
						$webp_file = system(get_template_directory() . '/bin/cwebp ' . $file_path . ' -o ' . str_ireplace('.png', '.webp', $file_path));
					}
					break;
			}
		}

		return $image_meta;
	}

	/**
	 * Deletes the webp files when a user removes images form the media library
	 * @param  Int $post_id ID of the attachement
	 */
	function delete_webp($post_id){
		$image_meta = wp_get_attachment_metadata($post_id);

		wp_delete_file(str_ireplace(array('.jpg', '.jpeg', '.png', '.gif'), '.webp', wp_upload_dir()['basedir'] . '/' . $image_meta['file']));
		foreach($image_meta['sizes'] as $size => $val){
			preg_match('/.*\//', $image_meta['file'], $date_dir, PREG_OFFSET_CAPTURE);
			$file_path = wp_upload_dir()['basedir'] . '/' . $date_dir[0][0] . $val['file'];
			wp_delete_file(str_ireplace(array('.jpg', '.jpeg', '.png', '.gif'), '.webp', $file_path));
		}
	}
}
