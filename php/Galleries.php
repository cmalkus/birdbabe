<?php

Galleries::init();

Class Galleries{
	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_filter('get_sample_permalink_html', array(__CLASS__, 'hide_permalinks'), 10, 5);
		add_action('save_post', array(__CLASS__, 'repeatable_meta_box_save'));
		add_action('add_meta_boxes', array(__CLASS__, 'add_meta_boxes'), 1);
	}

	/**
	 * Create the post type for the gallery.
	 */
	function create_post_type(){
		register_post_type('gallery',
			array(
				'labels' => array(
					'name' => __('Galleries', 'mvnp_basic_admin'),
					'singular_name' => __('Gallery', 'mvnp_basic_admin'),
					'menu_name' => __('Galleries', 'mvnp_basic_admin'),
				),
				'supports' => array('title'),
				'menu_icon' => 'dashicons-format-gallery',
				'public' => true,
				'publicly_queriable' => true,
				'show_in_nav_menus' => false,
				'exclude_from_search' => true,
				'has_archive' => false,
				'rewrite' => true,
			)
		);
	}

	function add_meta_boxes(){
		add_meta_box('repeatable-fields', __('Media', 'mvnp_basic_admin'), array(__CLASS__, 'repeatable_meta_box'), 'gallery', 'normal', 'default');
		add_meta_box('gallery-post-id', __('Usage', 'mvnp_basic_admin'), array(__CLASS__, 'gallery_id'), 'gallery', 'side', 'default');
	}

	/**
	 * This hides the permalink from the gallery post type
	 * @param  Object $return    The default permalink
	 * @param  Number $post_id   The post ID
	 * @param  string $new_title Title of the post
	 * @param  string $new_slug  Post Slug
	 * @param  Object $post      The post itself
	 * @return Object            The default permalink or nothing, if its a gallery post type
	 */
	function hide_permalinks($return, $post_id, $new_title, $new_slug, $post){
		if($post->post_type == 'gallery'){
			return '';
		}
		return $return;
	}

	/**
	 * Just spits out copyable text about how to use the gallery
	 */
	function gallery_id(){
		global $post;?>
		<div id="gallery-usage">
			<p><?php _e('Add the following to your .php template file:', 'mvnp_basic_admin');?></p>
			<pre>$context['gallery'] = Timber::get_post(<?php echo $post->ID ?>);</pre>
			<?php if($post->post_name){?>
				<p><?php _e('or:', 'mvnp_basic_admin');?></p>
				<pre>
$args = array(
	'post_type' => '<?php echo $post->post_type; ?>',
	'name' => '<?php echo $post->post_name; ?>',
);
$context['gallery'] = Timber::get_post($args);
				</pre>
			<?php }?>
			<p><?php _e('And in your .twig view file, you can access the images with:', 'mvnp_basic_admin');?></p>
			<pre>
{% for image in gallery.gallery_images %}
	<xmp><img src="{{ TimberImage(image.props).src('<?php _e('your-image-size', 'mvnp_basic_admin');?>') }}"></xmp>
{% endfor %}
			</pre>
		</div>
	<?php }

	/**
	 * This is the front end for the gallery fields. Here is where we enqueue the style and scripts to make the repeater run.
	 * The image props are kinda janky. They are saved as a php object, but need to be converted to JSON on the front end becuse using a php object in a text field destroys its usability
	 */
	function repeatable_meta_box(){
		global $post;
		$gallery_images = get_post_meta($post->ID, 'gallery_images', true);
		wp_nonce_field('repeatable_meta_box_nonce', 'repeatable_meta_box_nonce');
		require_once get_template_directory() . '/php/partials/gallery-view.php';
	}

	/**
	 * This saves our fields with the post.
	 * The image props are kinda janky though. They are saved as a php object, but need to be converted to JSON on the front end
	 * becuse using a php object in a text field destroys its usability
	 * @param  int $post_id The post we're working with
	 */
	function repeatable_meta_box_save($post_id){
		if(!isset($_POST['repeatable_meta_box_nonce']) || !wp_verify_nonce($_POST['repeatable_meta_box_nonce'], 'repeatable_meta_box_nonce')){
			return;
		}

		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
			return;
		}

		if(!current_user_can('edit_post', $post_id)){
			return;
		}

		$old = get_post_meta($post_id, 'gallery_images', true);
		$new = array();

		$names = $_POST['name'];
		$alts = $_POST['alt'];
		$properties = $_POST['props'];
		$links = $_POST['link'];
		$copys = $_POST['copy'];
		$iframes = $_POST['iframe'];
		$count = count($names);

		//die($count);

		for ($i = 0; $i < $count; $i++){
			$new[$i]['name'] = stripslashes(strip_tags($names[$i]));
			$new[$i]['alt'] = stripslashes(strip_tags($alts[$i]));
			$new[$i]['link'] = stripslashes(strip_tags($links[$i]));
			$new[$i]['copy'] = stripslashes(strip_tags($copys[$i]));
			$new[$i]['iframe'] = stripslashes(strip_tags($iframes[$i]));

			if(!empty($properties[$i])){
				$inputs = json_decode(stripslashes($properties[$i]));
				$filtered = array();

				if(is_array($inputs) || is_object($inputs)){
					foreach($inputs as $key => $value){
						$filtered[$key] = $value;
					}

					$new[$i]['props'] = $filtered;
				} else {
					$new[$i]['props'] = null;
				}
			}
		}
		if(!empty($new) && $new != $old){
			update_post_meta($post_id, 'gallery_images', $new);
		} elseif(empty($new) && $old){
			delete_post_meta($post_id, 'gallery_images', $old);
		}
	}
}
