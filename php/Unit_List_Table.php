<?php
namespace Units;
use WP_List_Table;

if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Unit_List_Table extends WP_List_Table{
	/**
	 * Constructor, we override the parent to pass our own arguments
	 * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
	 */
	function __construct(){
		parent::__construct(array(
			'singular'=> 'wp_list_unit',
			'plural' => 'wp_list_units',
			'ajax'   => false,
		));
	}

	/**
	 * Define the columns that are going to be used in the table
	 * @return array $columns, the array of columns to use with the table
	 */
	function get_columns() {
		return $columns= array(
			'available' => __('Avail.', 'mvnp_basic_admin'),
			'unit_number' => __('Unit', 'mvnp_basic_admin'),
			'affordable' => __('Afdbl.', 'mvnp_basic_admin'),
			'floor_plan' => __('Plan', 'mvnp_basic_admin'),
			'direction' => __('Dir.', 'mvnp_basic_admin'),
			'floor_number' => __('Floor', 'mvnp_basic_admin'),
			'living_area' => __('Living', 'mvnp_basic_admin'),
			'lanai_area' => __('Lanai', 'mvnp_basic_admin'),
			'parking' => __('Park', 'mvnp_basic_admin'),
			'bedrooms' => __('Bed', 'mvnp_basic_admin'),
			'bathrooms' => __('Bath', 'mvnp_basic_admin'),
			'price' => __('Price', 'mvnp_basic_admin'),
			'fp_file' => __('Floorplan File', 'mvnp_basic_admin'),
			'pano_file' => __('Panoramic File', 'mvnp_basic_admin'),
			'iso_file' => __('Isometric File', 'mvnp_basic_admin'),
			'map_file' => __('Map File', 'mvnp_basic_admin'),
		);
	}

	/**
	 * Decide which columns to activate the sorting functionality on
	 * @return array $sortable, the array of columns that can be sorted by the user
	 */
	public function get_sortable_columns(){
		return $sortable_columns = array(
			'available' => array('available', false),
			'unit_number' => array('unitNumber', true),
			'affordable' => array('affordable', false),
			'floor_plan' => array('floorPlan', false),
			'direction' => array('viewPlaneDirection', false),
			'floor_number' => array('level', false),
			'living_area' => array('livingSqFt', false),
			'lanai_area' => array('lanaiSqFt', false),
			'parking' => array('parking', false),
			'bedrooms' => array('bedrooms', false),
			'bathrooms' => array('bathrooms', false),
			'price' => array('price', false),
		);
	}

	/**
	 * Prepare the table with different parameters, pagination, columns and table elements
	 */
	function prepare_items() {
		global $wpdb, $_wp_column_headers, $unit_query;

		$screen = get_current_screen();
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);

		$query = $unit_query;
		$orderby = !empty($_GET['orderby']) ? $_GET['orderby'] : '';
		$order = !empty($_GET['order']) ? $_GET['order'] : 'ASC';
		$search = !empty($_GET['s']) ? $_GET['s'] : '';

		if(!empty($search)){
			$query .= " WHERE unitNumber LIKE '%$search%'";
		}

		if(!empty($orderby) && !empty($order)){
			$query .= ' ORDER BY ' . $orderby . ' ' . $order;
		}

		$columns = $this->get_columns();
		$_wp_column_headers[$screen->id] = $columns;

		$this->items = $wpdb->get_results($query);
	}

	/**
	 * Display the rows of records in the table
	 * @return string, echo the markup of the rows
	 */
	function display_rows(){
		// $this->display_tablenav( 'top' );
		$this->screen->render_screen_reader_content( 'heading_list' );
		$records = $this->items;
		$columns = $this->get_columns();
		?>
		<table id="unit-list" class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
			<thead>
				<tr>
					<?php $this->print_column_headers(); ?>
				</tr>
			</thead>

			<tbody>
			<?php
			if(!empty($records)){
				foreach($records as $rec){
					echo '<tr id="record-' . $rec->id . '" data-id="' . $rec->id . '">';
					foreach ($columns as $column_name => $column_display_name){
						switch($column_name){
							case 'available':
								echo '<td ' . 'class="' . $column_name . '">
									<input type="checkbox" id="available-' . $rec->id . '" name="available-' . $rec->id . '" ' . ($rec->available ? 'checked' : '') . '>
								</td>';
								break;
							case 'unit_number':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->unitNumber) . '</span>
									<input id="unitNumber-' . $rec->id . '" name="unitNumber-' . $rec->id . '" type="number" value="' . stripslashes($rec->unitNumber) . '">
								</td>';
								break;
							case 'affordable':
								echo '<td ' . 'class="' . $column_name . '">
									<span>' . (intval(stripslashes($rec->affordable)) < 0 ? '-' : (intval(stripslashes($rec->affordable)) > 0 ? 'Yes' : 'No')) . '</span>
									<input type="checkbox" id="affordable-' . $rec->id . '" name="affordable-' . $rec->id . '" ' . ($rec->affordable > 0 ? 'checked' : '') . '>
								</td>';
								break;
							case 'floor_plan':
								echo '<td ' . 'class="' . $column_name . '">
									<span>' . stripslashes($rec->floorPlan) . '</span>
									<input id="floorPlan-' . $rec->id . '" name="floorPlan-' . $rec->id . '" type="text" value="' . stripslashes($rec->floorPlan) . '">
								</td>';
								break;
							case 'direction':
								echo '<td ' . 'class="' . $column_name . '">
									<span>' . stripslashes($rec->viewPlaneDirection) . '</span>
									<input id="viewPlaneDirection-' . $rec->id . '" name="viewPlaneDirection-' . $rec->id . '" type="text" value="' . stripslashes($rec->viewPlaneDirection) . '">
								</td>';
								break;
							case 'floor_number':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->level) . '</span>
									<input id="level-' . $rec->id . '" name="level-' . $rec->id . '" type="number" value="' . stripslashes($rec->level) . '">
								</td>';
								break;
							case 'living_area':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->livingSqFt) . ' ft.<sup>2</sup></span>
									<input id="livingSqFt-' . $rec->id . '" name="livingSqFt-' . $rec->id . '" type="number" value="' . stripslashes($rec->livingSqFt) . '">
								</td>';
								break;
							case 'lanai_area':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->lanaiSqFt) . ' ft.<sup>2</sup></span>
									<input id="lanaiSqFt-' . $rec->id . '" name="lanaiSqFt-' . $rec->id . '" type="number" value="' . stripslashes($rec->lanaiSqFt) . '">
								</td>';
								break;
							case 'parking':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->parking) . '</span>
									<input id="parking-' . $rec->id . '" name="parking-' . $rec->id . '" type="number" value="' . stripslashes($rec->parking) . '">
								</td>';
								break;
							case 'bedrooms':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->bedrooms) . '</span>
									<input id="bedrooms-' . $rec->id . '" name="bedrooms-' . $rec->id . '" type="number" value="' . stripslashes($rec->bedrooms) . '">
								</td>';
								break;
							case 'bathrooms':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . stripslashes($rec->bathrooms) . '</span>
									<input id="bathrooms-' . $rec->id . '" name="bathrooms-' . $rec->id . '" type="number" value="' . stripslashes($rec->bathrooms) . '">
								</td>';
								break;
							case 'price':
								echo '<td ' . 'class="right-align ' . $column_name . '">
									<span>' . (stripslashes($rec->price) > 0 ? '$' . number_format(stripslashes($rec->price), 0) : '-') . '</span>
									<input id="price-' . $rec->id . '" name="price-' . $rec->id . '" type="number" value="' . stripslashes($rec->price) . '">
								</td>';
								break;
							case 'fp_file':
								echo '<td ' . 'class="' . $column_name . '"' . '>
									<span>' . stripslashes($rec->floorPlanFile) . '</span>
									<input id="floorPlanFile-' . $rec->id . '" name="floorPlanFile-' . $rec->id . '" type="text" value="' . stripslashes($rec->floorPlanFile) . '">
								</td>';
								break;
							case 'pano_file':
								echo '<td ' . 'class="' . $column_name . '"' . '>
									<span>' . stripslashes($rec->panoramicViewFile) . '</span>
									<input id="panoramicViewFile-' . $rec->id . '" name="panoramicViewFile-' . $rec->id . '" type="text" value="' . stripslashes($rec->panoramicViewFile) . '">
								</td>';
								break;
							case 'iso_file':
								echo '<td ' . 'class="' . $column_name . '"' . '>
									<span>' . stripslashes($rec->isometricFile) . '</span>
									<input id="isometricFile-' . $rec->id . '" name="isometricFile-' . $rec->id . '" type="text" value="' . stripslashes($rec->isometricFile) . '">
								</td>';
								break;
							case 'map_file':
								echo '<td ' . 'class="' . $column_name . '"' . '>
									<span>' . stripslashes($rec->floorMapFile) . '</span>
									<input id="floorMapFile-' . $rec->id . '" name="floorMapFile-' . $rec->id . '" type="text" value="' . stripslashes($rec->floorMapFile) . '">
								</td>';
								break;
							default:
								break;
						}
					}
					echo'</tr>';
				}
			} ?>
			</tbody>
			<tfoot>
				<tr>
					<?php $this->print_column_headers(false); ?>
				</tr>
			</tfoot>
		</table>
		<?php
		$this->display_tablenav( 'bottom' );
	}
}
