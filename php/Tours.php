<?php

Tours::init();

Class Tours{

	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_action('init', array(__CLASS__, 'create_taxonomy'));
		add_action('init', array(__CLASS__, 'create_type_taxonomy'));

		add_filter('views_edit-tour', array(__CLASS__, 'tours_copy_textarea'));
		add_action('wp_ajax_tour_info_save', array(__CLASS__, 'tour_info_meta_box_save'));

	}

	/**
	 * Create the post type for FAQs.
	 */
	function create_post_type(){
		register_post_type('tour',
			array(
				'labels' => array(
					'name' => __('Tours', 'mvnp_basic_admin'),
					'singular_name' => __('Tour', 'mvnp_basic_admin'),
					'archives' => __('Tours', 'mvnp_basic'),
					'menu_name' => __('Tours', 'mvnp_basic_admin'),
				),
				'hierarchical' => true,
				'supports' => array('title', 'editor', 'page-attributes', 'thumbnail'),
				'menu_icon' => 'dashicons-palmtree',
				'public' => true,
				'has_archive' => true,
				'publicly_queriable' => true,
				'rewrite' => array('slug' => 'tours'),
				'show_in_nav_menus' => true,
			)
		);
	}

	/**
	 * Create tours category taxonomy
	 */
	function create_taxonomy(){
		register_taxonomy(
			'tour-category',
			'tour',
			array(
				'label' => __('Tour Island', 'mvnp_basic_admin'),
				'rewrite' => true,
				'hierarchical' => false,
				'show_in_nav_menus' => false,
			)
		);
	}

	/**
	 * Create tours category taxonomy
	 */
	function create_type_taxonomy(){
		register_taxonomy(
			'tour-type',
			'tour',
			array(
				'label' => __('Tour Type', 'mvnp_basic_admin'),
				'rewrite' => true,
				'hierarchical' => false,
				'show_in_nav_menus' => false,
			)
		);
	}

	/**
	 * Saves the values to theme mod on save
	 * @return null         null
	 */
	function tour_info_meta_box_save(){
		if(!isset($_POST['data']['tour_list_meta_box_nonce']) || !wp_verify_nonce($_POST['data']['tour_list_meta_box_nonce'], 'tour_list_meta_box_nonce')){
			return;
		}

		if(!current_user_can('activate_plugins') || !current_user_can('edit_theme_options')){
			return;
		}

		set_theme_mod('tours_copy', urldecode($_POST['data']['tours_copy']));
	}

	/**
	 * Adds the import calendar button on tours page
	 * @param  string $screen the name of the current wp admin view
	 */
	function tours_copy_textarea($screen){
		$screen = get_current_screen();
		$tour_info = get_theme_mods();

		if($screen->post_type == 'tour'){
			wp_nonce_field('tour_list_meta_box_nonce', 'tour_list_meta_box_nonce');
			echo '<div class="clearfix">';
			echo '<label id="tours-copy">' . __('Tours Copy', 'mvnp_basic_admin');
			echo '<textarea id="tours_copy" class="widefat copy-input i18n-multilingual" name="tours_copy" rows="5">' .  (esc_attr($tour_info['tours_copy']) != '' ? esc_attr($tour_info['tours_copy']) : '') . '</textarea>';
			echo '</label>';
			echo '</div>';
			echo '<button class="button button-primary save-tour-copy" type="button" title="' . __('Save tour Copy', 'mvnp_basic_admin') . '">' . __('Save Tour Copy', 'mvnp_basic_admin') . '</button><hr>';
		}
	}
}
