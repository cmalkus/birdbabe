<?php

class Tours_Widget extends WP_Widget{
	function __construct(){
		parent::__construct(false, __('Tours', 'mvnp_basic_admin'), array(
			'description' => __('Post data for tours', 'mvnp_basic_admin'),
		));
	}

	public function widget($args, $instance){
		$title = apply_filters('widget_title', $instance['content']);
		$tour_post = get_post(apply_filters('widget_title', $instance['post']));

		echo $args['before_widget'];
		if(!empty($title)){
			echo $args['before_title'] . $title . $args['after_title'];
		}

		if($tour_post->ID != 0){
			echo '<a class="btn ' . ($args['id'] != 'sidebar-cta' ? 'btn-inverted' : 'btn-green') . '" href="' . get_permalink($tour_post) . '">' . __('Get the details!', 'mvnp_basic') . '</a>';
		}
		echo $args['after_widget'];
	}

	public function update($new_instance, $old_instance) {
		$instance = array();
		$instance['content'] = (!empty($new_instance['content'])) ? strip_tags($new_instance['content']) : '';
		$instance['post'] = (!empty($new_instance['post'])) ? $new_instance['post'] : '';

		return $instance;
	}

	public function form($instance){
		if (isset($instance['content'])){
			$title = $instance['content'];
		}else{
			$title = __('New content', 'mvnp_basic_admin');
		}

		if (isset($instance['post'])){
			$tour_post = $instance['post'];
		}else{
			$tour_post = 0;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('content'); ?>"><?php _e('Content', 'mvnp_basic_admin'); ?></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>"><?php echo esc_attr($title); ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('post'); ?>"><?php _e('Tour', 'mvnp_basic_admin'); ?></label>
			<?php
			wp_dropdown_pages(array(
				'orderby' => 'title',
				'post_type' => 'tour',
				'name' => $this->get_field_name('post'),
				'id' => $this->get_field_id('post'),
				'selected' => $tour_post,
				'option_none_value' => '0',
				'show_option_none' => '- ' . __('Select', 'mvnp_basic_admin') . ' -',
			));
			?>
		</p>
		<?php
	}
}
