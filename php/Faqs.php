<?php

Faqs::init();

Class Faqs{

	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_shortcode('faq', array(__CLASS__, 'create_shortcode'));
	}

	/**
	 * Create the post type for FAQs.
	 */
	function create_post_type(){
		register_post_type('faq',
			array(
				'labels' => array(
					'name' => __('FAQs', 'mvnp_basic_admin'),
					'singular_name' => __('FAQ', 'mvnp_basic_admin'),
					'archives' => __('FAQs', 'mvnp_basic'),
					'menu_name' => __('FAQs', 'mvnp_basic_admin'),
				),
				'hierarchical' => true,
				'supports' => array('title', 'editor', 'page-attributes'),
				'menu_icon' => 'dashicons-editor-help',
				'public' => true,
				'has_archive' => true,
				'publicly_queriable' => true,
				'rewrite' => array('slug' => 'faqs'),
			)
		);
	}

	/**
	 * Function for the faq shortcode. shortcode is formatted like this: [faq id="XXX" type="{content/links}"]
	 * @param  [array] $atts the attributes for the shortcode. takes id and type. id must be a valid faq id and type must be either "content" or "links"
	 * @return [string]      html string of the faqs
	 */
	function create_shortcode($atts) {
		$out = '';
		$a = shortcode_atts(array(
			'id' => '0',
			'type' => 'content',
		), $atts);

		if(get_post_type($a['id']) !== 'faq'){
			return _e('Error: Post is not an FAQ', 'mvnp_basic');
		}

		$children = get_children($a['id']);

		if(count($children) < 1){
			return _e('Error: FAQ has no children', 'mvnp_basic');
		}

		if($a['type'] === 'content'){
			$out .= '<ul>' . "\r\n";

			foreach($children as $child){
				$out .= "\t" . '<li class="faq-slide-content" itemscope itemtype="http://schema.org/Question">' . "\r\n";
				$out .= "\t\t" . '<h4 itemprop="name">' . $child->post_title . '</h4>' . "\r\n";
				$out .= "\t\t" . '<div itemprop="suggestedAnswer acceptedAnswer" itemscope itemtype="http://schema.org/Answer">' . $child->post_content . '</div>' . "\r\n";
				$out .= '</li>' . "\r\n";
			}

			$out .= '</ul>' . "\r\n";
		}else if($a['type'] === 'links'){
			$out .= '<ul>' . "\r\n";

			foreach($children as $child){
				$out .= "\t" . '<li itemscope itemtype="http://schema.org/Question">' . "\r\n";
				$out .= "\t\t" . '<a href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a>' . "\r\n";
				$out .= "\t" . '</li>' . "\r\n";
			}

			$out .= '</ul>' . "\r\n";
		}else{
			return _e('Error: Type must be "links" or "content"', 'mvnp_basic');
		}

		return $out;
	}
}
