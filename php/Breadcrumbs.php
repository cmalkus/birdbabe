<?php

/**
 * Comprehensive breadcrumbs, via Stuart at thewebtaylor.com
 * https://www.thewebtaylor.com/articles/wordpress-creating-breadcrumbs-without-a-plugin
 * this has been heavily modified. do not rely on the original as a fix if something goes wrong
 * @param  string $separator  The thing that goes between page titles
 * @param  string $home_title Name the homepage something other than its title
 */
Class Breadcrumbs{
	public function create($separator, $home_title){
		$breadcrumbs_id = 'breadcrumbs';
		$breadcrumbs_class = 'breadcrumbs';
		$custom_taxonomy = '';
		$output = '';

		global $post, $wp_query, $wp;

		// Do not display on the homepage
		if(!is_front_page()){
			// Build the breadcrumbs
			$output .= '<ul id="' . $breadcrumbs_id . '" class="' . $breadcrumbs_class . ' container" itemscope itemtype="http://schema.org/BreadcrumbList">';
			// Home page
			$output .= '<li class="item-home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '" itemprop="item">' . $home_title . '</a><meta itemprop="position" content="1"><meta itemprop="name" content="' . $home_title . '"></li>';
			$output .= '<li class="separator separator-home"> ' . $separator . ' </li>';
			if(is_archive() && !is_tax() && !is_category() && !is_tag() && !is_day() && !is_month() && !is_year()){
				if(is_author()){
					// Auhor archive
					// Display author name
					$output .= '<li class="item-current item-current-' .  get_the_author_meta('user_nicename') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-current-' .  get_the_author_meta('user_nicename') . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' .  get_the_author_meta('display_name') . '" itemprop="item">' . __('Author:', 'mvnp_basic') . ' ' .  get_the_author_meta('display_name') . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . __('Author:', 'mvnp_basic') . ' ' .  get_the_author_meta('display_name') . '"></li>';
				}else{
					$output .= '<li class="item-current item-archive" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-archive" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . post_type_archive_title($prefix, false) . '" itemprop="item">' . post_type_archive_title($prefix, false) . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . post_type_archive_title($prefix, false) . '"></li>';
				}
			} else if(is_archive() && is_tax() && !is_category() && !is_tag()){
				// If post is a custom post type
				$post_type = get_post_type();
				// If it is a custom post type display name and link
				if($post_type != 'post'){
					$post_type_object = get_post_type_object($post_type);
					$post_type_archive = get_post_type_archive_link($post_type);
					$output .= '<li class="item-cat item-custom-post-type-' . $post_type . '" itemscope itemtype="http://schema.org/ListItem"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '" itemprop="item">' . $post_type_object->labels->name . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . $post_type_object->labels->name . '"></li>';
					$output .= '<li class="separator"> ' . $separator . ' </li>';
				}
				$custom_tax_name = get_queried_object()->name;
				$output .= '<li class="item-current item-archive" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-archive" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . $custom_tax_name . '" itemprop="item">' . $custom_tax_name . '</a><meta itemprop="position" content="3"><meta itemprop="name" content="' . $custom_tax_name . '"></li>';
			} else if(is_single()){
				// If post is a custom post type
				$post_type = get_post_type();
				// If it is a custom post type display name and link
				if($post_type != 'post'){
					$post_type_object = get_post_type_object($post_type);
					$post_type_archive = get_post_type_archive_link($post_type);
					$output .= '<li class="item-cat item-custom-post-type-' . $post_type . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '" itemprop="item">' . $post_type_object->labels->name . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . $post_type_object->labels->name . '"></li>';
					$output .= '<li class="separator"> ' . $separator . ' </li>';

					if($post->post_parent){
						// If child page, get parents
						$anc = get_post_ancestors($post->ID);
						// Get parents in the right order
						$anc = array_reverse($anc);
						// Parent page loop
						if(!isset($parents)){
							$parents = null;
						}

						foreach($anc as $key => $ancestor){
							$tmp = 2 + $key;
							$parents .= '<li class="item-parent item-parent-' . $ancestor . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '" itemprop="item">' . get_the_title($ancestor) . '</a><meta itemprop="position" content="' . $tmp . '"><meta itemprop="name" content="' . get_the_title($ancestor) . '"></li>';
							$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
						}
						// Display parent pages
						$output .= $parents;
					}
				}

				// Get post category info
				$category = get_the_category();
				if(!empty($category)){
					// Get last category post is in
					$last_category = end(array_values($category));
					// Get parent any categories and create array
					$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','), ',');
					$cat_parents = explode(',', $get_cat_parents);
					// Loop through parent categories and store in variable $cat_display
					$cat_display = '';
					foreach($cat_parents as $key => $parents){
						$tmp = $key + 2;
						$cat_display .= '<li class="item-cat">' . $parents . '<meta itemprop="position" content="' . $tmp . '"><meta itemprop="name" content="' . $parents . '"></li>';
						$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
					}
				}

				// If it's a custom post type within a custom taxonomy
				$taxonomy_exists = taxonomy_exists($custom_taxonomy);
				if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists){
					$taxonomy_terms = get_the_terms($post->ID, $custom_taxonomy);
					$cat_id = $taxonomy_terms[0]->term_id;
					$cat_nicename = $taxonomy_terms[0]->slug;
					$cat_link = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
					$cat_name = $taxonomy_terms[0]->name;
				}

				// Check if the post is in a category
				if(!empty($last_category)){
					$output .= $cat_display;
					$output .= '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-' . $post->ID . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_title() . '" itemprop="item">' . get_the_title() . '</a><meta itemprop="position" content="' . ($tmp + 1) . '"><meta itemprop="name" content="' . get_the_title() . '"></li>';
					// Else if post is in a custom taxonomy
				} else if(!empty($cat_id)){
					$output .= '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '" itemprop="item">' . $cat_name . '</a><meta itemprop="position" content="' . ($tmp + 1) . '"><meta itemprop="name" content="' . $cat_name . '"></li>';
					$output .= '<li class="separator"> ' . $separator . ' </li>';
					$output .= '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-' . $post->ID . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_title() . '" itemprop="item">' . get_the_title() . '</a><meta itemprop="position" content="' . ($tmp + 2) . '"><meta itemprop="name" content="' . get_the_title() . '"></li>';
				} else {
					$output .= '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-' . $post->ID . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_title() . '" itemprop="item">' . get_the_title() . '</a><meta itemprop="position" content="' . ($tmp + 1) . '"><meta itemprop="name" content="' . get_the_title() . '"></li>';
				}

			} else if(is_category()){
				// Category page
				$output .= '<li class="item-current item-cat" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-cat" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . single_cat_title('', false) . '" itemprop="item">' . single_cat_title('', false) . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . single_cat_title('', false) . '"></li>';
			} else if(is_page()){
				// Standard page
				if($post->post_parent){
					// If child page, get parents
					$anc = get_post_ancestors($post->ID);
					// Get parents in the right order
					$anc = array_reverse($anc);
					// Parent page loop
					if(!isset($parents)){
						$parents = null;
					}

					foreach($anc as $key => $ancestor){
						$tmp = (2 + $key);
						$parents .= '<li class="item-parent item-parent-' . $ancestor . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '" itemprop="item">' . get_the_title($ancestor) . '</a><meta itemprop="position" content="' . $tmp . '"><meta itemprop="name" content="' . get_the_title($ancestor) . '"></li>';
						$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
					}
					// Display parent pages
					$output .= $parents;
					// Current page
					$output .= '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_title() . '" itemprop="item">' . get_the_title() . '</a><meta itemprop="position" content="' . (1 + $tmp) . '"><meta itemprop="name" content="' . get_the_title() . '"></li>';
				} else {
					// Just display current page if not parents
					$output .= '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-' . $post->ID . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_title() . '" itemprop="item">' . get_the_title() . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . get_the_title() . '"></li>';
				}

			} else if(is_tag()){
				// Tag page
				// Get tag information
				$term_id = get_query_var('tag_id');
				$taxonomy = 'post_tag';
				$args = 'include=' . $term_id;
				$terms = get_terms($taxonomy, $args);
				$get_term_id = $terms[0]->term_id;
				$get_term_slug = $terms[0]->slug;
				$get_term_name = $terms[0]->name;
				// Display the tag name
				$output .= '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . $get_term_name . '" itemprop="item">' . $get_term_name . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . $get_term_name . '"></li>';
			} elseif(is_day()){
				// Day archive
				// Year link
				$output .= '<li class="item-year item-year-' . get_the_time('Y') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '" itemprop="item">' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '"></li>';
				$output .= '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
				// Month link
				$output .= '<li class="item-month item-month-' . get_the_time('m') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '" title="' . get_the_time('M') . '" itemprop="item">' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '</a><meta itemprop="position" content="3"><meta itemprop="name" content="' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '"></li>';
				$output .= '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
				// Day display
				$output .= '<li class="item-current item-' . get_the_time('j') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-' . get_the_time('j') . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_time('jS') . ' ' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '" itemprop="item">' . get_the_time('jS') . ' ' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '</a><meta itemprop="position" content="4"><meta itemprop="name" content="' . get_the_time('jS') . ' ' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '"></li>';
			} else if(is_month()){
				// Month Archive
				// Year link
				$output .= '<li class="item-year item-year-' . get_the_time('Y') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '" itemprop="item">' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '"></li>';
				$output .= '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
				// Month display
				$output .= '<li class="item-month item-month-' . get_the_time('m') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_time('M') . '" itemprop="item">' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '</a><meta itemprop="position" content="3"><meta itemprop="name" content="' . get_the_time('M') . ' ' . __('Archives', 'mvnp_basic') . '"></li>';
			} else if(is_year()){
				// Display year archive
				$output .= '<li class="item-current item-current-' . get_the_time('Y') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '" itemprop="item">' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . get_the_time('Y') . ' ' . __('Archives', 'mvnp_basic') . '"></li>';
			} else if(get_query_var('paged')){
				// Paginated archives
				$output .= '<li class="item-current item-current-' . get_query_var('paged') . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-current-' . get_query_var('paged') . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . __('Page', 'mvnp_basic') . ' ' . get_query_var('paged') . '" itemprop="item">' . __('Page', 'mvnp_basic') . ' ' . get_query_var('paged') . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . __('Page', 'mvnp_basic') . ' ' . get_query_var('paged') . '"></li>';
			} else if(is_search()){
				// Search results page
				$output .= '<li class="item-current item-current-' . get_search_query() . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-current-' . get_search_query() . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . sprintf(__('Search results for %s', 'mvnp_basic'), get_search_query()) . '" itemprop="item">' . sprintf(__('Search results for %s', 'mvnp_basic'), get_search_query())  . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . sprintf(__('Search results for %s', 'mvnp_basic'), get_search_query()) . '"></li>';
			} else if(is_home()){
				$output .= '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="bread-current bread-' . $post->ID . '" href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . get_the_title(get_option('page_for_posts', true)) . '" itemprop="item">' . get_the_title(get_option('page_for_posts', true)) . '</a><meta itemprop="position" content="2"><meta itemprop="name" content="' . get_the_title(get_option('page_for_posts', true)) . '"></li>';
			} elseif(is_404()){
				// 404 page
				$output .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . home_url(add_query_arg(array(),$wp->request)) . '" title="' . __('Error 404', 'mvnp_basic') . '" itemprop="item">' . __('Error 404', 'mvnp_basic') . '</a><meta itemprop="position" content="1"><meta itemprop="name" content="' . __('Error 404', 'mvnp_basic') . '"></li>';
			}
			$output .= '</ul>';
		}

		return $output;
	}
}
