<?php
/**
 * [Sort_Posts Sort Posts Class]
 */
class Sort_Posts {
	var $order, $orderby;

	function __construct($orderby, $order){
		$this->orderby = $orderby;
		$this->order = ('desc' == strtolower($order)) ? 'DESC' : 'ASC';
	}

	function sort($a, $b){
		if($a->{$this->orderby} == $b->{$this->orderby}){
			return 0;
		}

		if($a->{$this->orderby} < $b->{$this->orderby}){
			return ('ASC' == $this->order) ? -1 : 1;
		} else {
			return ('ASC' == $this->order) ? 1 : -1;
		}
	}
}
