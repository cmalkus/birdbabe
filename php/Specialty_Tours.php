<?php

Specialty_Tours::init();

Class Specialty_Tours{

	public function __construct(){}

	public function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
	}

	/**
	 * Create the post type for FAQs.
	 */
	function create_post_type(){
		register_post_type('specialty-tour',
			array(
				'labels' => array(
					'name' => __('Specialty Tours', 'mvnp_basic_admin'),
					'singular_name' => __('Specialty Tour', 'mvnp_basic_admin'),
					'archives' => __('Specialty Tours', 'mvnp_basic'),
					'menu_name' => __('Specialty Tours', 'mvnp_basic_admin'),
				),
				'hierarchical' => true,
				'supports' => array('title', 'editor', 'page-attributes', 'thumbnail'),
				'menu_icon' => 'dashicons-location-alt',
				'public' => true,
				'has_archive' => true,
				'publicly_queriable' => true,
				'rewrite' => array('slug' => 'specialty-tours'),
			)
		);
	}
}
