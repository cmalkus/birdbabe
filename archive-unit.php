<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

global $wp, $wpdb, $unit_query, $post;

$units_raw = $wpdb->get_results($unit_query);
$units = group_by_object_key($units_raw, 'viewPlaneDirection');

foreach($units as $direction => &$value){
	$units[$direction] = group_by_object_key(array_reverse($value), 'level');

	foreach($value as $floor => &$obj){
		uasort($obj, function($a, $b){
			if($a->unitNumber == $b->unitNumber){
				return 0;
			}

			return ($a->unitNumber > $b->unitNumber) ? -1 : 1;
		});
	}
}

$context = Timber::context();

$context['unit_manager'] = new Units\Units;
$context['sidebar'] = false;
$context['title'] = __('All Units', 'mvnp_basic');
$context['units'] = $units;
$context['post'] = new stdClass();
$context['post']->post_content = get_theme_mod('all_unit_info');
$context['post']->title = $context['title'];
$context['post']->link = home_url(add_query_arg(array(), $wp->request));
$context['post']->type = 'page';

Timber::render('archive-unit.twig', $context);
