<?php
/**
 * The Template for displaying all single unit posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $wp, $wpdb, $unit_query, $post;

$query = $unit_query . " WHERE unitNumber = $post->unit_number";
$unit_info = $wpdb->get_results($query)[0];
$formatted = Units\Units::parse_unit_content(get_theme_mod('single_unit_info'), $unit_info);

$context = Timber::context();

$context['modal'] = array_key_exists('modal', $_GET);
$context['breadcrumbs'] = $context['modal'] ? false : $context['breadcrumbs'];
$context['title'] = sprintf(__('Unit %s', 'mvnp_basic'), $post->unit_number);
$context['post'] = new stdClass();
$context['post'] = $unit_info;
$context['post']->post_content = $formatted;
$context['post']->title = $context['title'];
$context['post']->link = home_url(add_query_arg(array(), $wp->request));
$context['post']->type = 'unit';
$context['post']->seo_type = 'http://www.productontology.org/id/Condominium';
$context['post']->slug = $post->unit_number;

Timber::render('single-unit.twig', $context);
