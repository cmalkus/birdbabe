<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();

$context['post'] = new stdClass();
$context['post']->title = __('Page not found', 'mvnp_basic');
$context['post']->link = home_url(add_query_arg(array(), $wp->request));
$context['post']->type = 'page';
$context['post']->post_content = __('The page you are looking for cannot found', 'mvnp_basic');

Timber::render('404.twig', $context);
