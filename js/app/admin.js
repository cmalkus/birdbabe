/* globals siteUrl: true, templateUrl: true, google: false, __: false, qTranslateX: false, qTranslateConfig: false, wp: false, eventAddress: false, repeating: true, recurrence: true, noLocation: true, postId: false */

/**
 * Grabs the ULR parameters and returns an object
 * @param  {string} [query=window.location.search.substring(1)] Fully formatted URL, in case you dont want to use the current pages params
 * @return {object} URL params returned as an object
 */
function urlParams(query){
	var queryString = {},
		vars, pair, i, arr;

	query = query ? query.indexOf('?') === 0 ? query.split('?')[1] : query : window.location.search.substring(1);
	vars = query.split('&');

	for(i = 0; i < vars.length; i++){
		pair = vars[i].split('=');

		if(typeof queryString[pair[0]] === 'undefined'){
			queryString[pair[0]] = decodeURIComponent(pair[1]);
			// If second entry with this name
		}else if(typeof queryString[pair[0]] === 'string'){
			arr = [queryString[pair[0]], decodeURIComponent(pair[1])];
			queryString[pair[0]] = arr;
			// If third or later entry with this name
		}else{
			queryString[pair[0]].push(decodeURIComponent(pair[1]));
		}
	}

	return queryString;
}

/**
 * Debounces your function. ie waits for the the end of input before executing
 * @param  {Function} fn The function you want to debounce
 * @param  {Number} delay The amount of time before executing (in ms)
 * @return {Function} Returns the function with a timout
 */
function debounce(fn, delay){
	var timer = null;

	return function(){
		var context = this,
			args = arguments;

		clearTimeout(timer);
		timer = setTimeout(function(){
			fn.apply(context, args);
		}, delay);
	};
}

/**
 * Makes an ajax post to the import_google_cal function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function importGoogleCalendar(data, _callback){
	data = {
		action: 'import_google_cal'
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		data: data,
		success: function(data){
			location.reload();
		}
	});
}

/**
 * Makes an ajax post to the save_units function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function saveUnitInfo(data, _callback){
	data = {
		action: 'save_units',
		data: data
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		data: data,
		success: function(data){
			location.reload();
		}
	});
}

/**
 * Makes an ajax post to the save_units function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function updateUnitEntry(data, _callback){
	data = {
		action: 'update_unit_entry',
		data: data
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		data: data,
		success: function(data){
			if(typeof _callback === 'function') return _callback(data);
		}
	});
}

/**
 * Attaches google map to a DOM element
 * @param  {Object} elem The HTML element that you want to attach the map to
 * @param  {string} [name='My Location'] Name on the marker
 * @param  {Object} coords Object containing lat and lng strings
 * @return {Boolean} Returns false if required params are missing.
 */
function initMap(elem, name, coords){
	var map, marker;

	name = name || __('My Location', 'mvnp_basic_admin');
	map = new google.maps.Map(elem, {
		center: coords,
		zoom: 18,
		scrollwheel: false
	});

	marker = new google.maps.Marker({
		position: coords,
		map: map,
		title: name
	});

	return true;
}

/**
 * Codes an addrress string to a lat/lng object and sends it to the map making function
 * @param  {Object} elem The HTML element that you want to attach the map to. Passed to initMap
 * @param  {string} [name='My Location'] Name on the marker. Passed to initMap
 * @param  {string} address Address string you want to encode
 * @return {Boolean|Object} Google geocoder object. Returns false if required params are missing.
 */
function codeAddress(elem, name, address, makeMap){
	var geocoder = new google.maps.Geocoder();

	name = name || __('My Location', 'mvnp_basic_admin');
	return geocoder.geocode({
		'address': address
	}, function(results, status){
		if(status === google.maps.GeocoderStatus.OK && makeMap){
			initMap(elem, name, {
				lat: results[0].geometry.location.lat(),
				lng: results[0].geometry.location.lng()
			});
		}
	});
}

jQuery(document).ready(function($){
	/**
	 * Pulls up the in-post WP media manager and sets values for the media object when selected
	 * @param  {object} self The Scope of the element that was clicked
	 */
	function makeMediaGallery(self){
		if(!self) return;

		var metaImageFrame,
			mediaAttachment;

		if(metaImageFrame){
			metaImageFrame.open();
			return;
		}

		metaImageFrame = wp.media.frames.metaImageFrame = wp.media({
			// title: meta_image.title,
			// button: { text:  meta_image.button },
			// library: { type: 'image' }
		});

		metaImageFrame.on('select', function(){
			if(typeof qTranslateConfig === 'object'){
				var qtx = qTranslateConfig.qtx;

				mediaAttachment = metaImageFrame.state().get('selection').first().toJSON();
				jQuery(self).nextAll('.custom-media-url').html(JSON.stringify(mediaAttachment));
				jQuery(self).attr('src', mediaAttachment.sizes.thumbnail.url);

				qtx.removeContentHook(jQuery(self).parent().next().find('.name-input')[0]);
				jQuery(self).parent().next().find('.name-input').val(mediaAttachment.title);
				qtx.addContentHook(jQuery(self).parent().next().find('.name-input')[0]);

				qtx.removeContentHook(jQuery(self).parent().next().find('.alt-input')[0]);
				jQuery(self).parent().next().find('.alt-input').val(mediaAttachment.alt);
				qtx.addContentHook(jQuery(self).parent().next().find('.alt-input')[0]);

				qtx.removeContentHook(jQuery(self).parent().next().find('.copy-input')[0]);
				jQuery(self).parent().next().find('.copy-input').val(mediaAttachment.caption);
				qtx.addContentHook(jQuery(self).parent().next().find('.copy-input')[0]);
			}
		});

		metaImageFrame.open();
	}

	// TODO with gutenberg, the dom takes forever to load and even with this in window.load it misses the element.
	// need to find some other place to hook this is. also they changed the names of the selects so not even sure if the parent
	// select id will even be the same each time.
	$('#inspector-select-control-2').on('change', function(){
		var that = $(this),
			data = {
				action: 'get_page_templates',
				data: {
					post_id: postId,
					post_parent: that.val()
				}
			};

		$.ajax({
			type: 'GET',
			url: siteUrl + '/wp-admin/admin-ajax.php',
			data: data,
			success: function(data){
				var out = '<option value="default">Default Template</option>',
					name;

				data = JSON.parse(data);
				for(name in data){
					out += '<option value="' + data[name] + '">' + name + '</option>';
				}

				$('.editor-page-attributes__template select').html(out);
			}
		});
	});

	$('#repeatable-fieldset-one .ui-sortable').sortable({
		handle: '.drag-handle',
		placeholder: 'drag-placeholder'
	});

	$('.import-google-cal').on('click', function(e){
		e.preventDefault();
		importGoogleCalendar();
	});

	if($('#event-location-map').length && eventAddress){
		codeAddress($('#event-location-map')[0], 'My event location', eventAddress, true);
	}

	if(typeof repeating !== 'undefined'){
		$('#repeating-properties').show();

		if(recurrence.FREQ === 'DAILY'){
			$('#interval-wrapper').hide();
		}

		if(recurrence.FREQ === 'WEEKLY'){
			$('#recurr_days').show();
		}

		if(recurrence.FREQ === 'MONTHLY'){
			$('#recurr_by').show();
		}
	}

	if(typeof noLocation !== 'undefined' && !noLocation){
		$('#event_location').slideDown();
	}

	$('.location-input').on('input', debounce(function(){
		if($(this).val()){
			codeAddress($('#event-location-map')[0], 'My event location', $(this).val(), true);
		}
	}, 750));

	$('#all_day').on('change', function(){
		var that = $(this);

		$('.event-time').each(function(){
			$(this).attr('disabled', that.prop('checked'));
		});
	});

	$('#recurring').on('change', function(){
		var that = $(this);
		repeating = that.prop('checked');

		if(repeating){
			$('#repeating-properties').slideDown();
		}else{
			$('#repeating-properties').slideUp();
		}
	});

	$('#noLocation').on('change', function(){
		var that = $(this);
		noLocation = that.prop('checked');

		if(!noLocation){
			$('#event_location').slideDown();
		}else{
			$('#event_location').slideUp();
		}
	});

	$('#freq').on('change', function(){
		var that = $(this);

		if(that.val() === 'DAILY'){
			$('#interval-wrapper').hide();
		}else{
			$('#interval-wrapper').show();
		}

		if(that.val() === 'WEEKLY'){
			$('#recurr_days').show();
		}else{
			$('#recurr_days').hide();
		}

		if(that.val() === 'MONTHLY'){
			$('#recurr_by').show();
		}else{
			$('#recurr_by').hide();
		}
	});

	/**
	 * Adds a new item to the gallery manager. Sets name attribtes to the newly created row elements
	 */
	$('#add-row').on('click', function(){
		var row = document.createElement('tr');

		row = $(row);

		row.html($('.empty-row.screen-reader-text').html());
		row.attr('name', 'item[]');
		row.find('.custom-media-url').attr('name', 'props[]');
		row.find('.name-input').attr('name', 'name[]');
		row.find('.alt-input').attr('name', 'alt[]');
		row.find('.link-input').attr('name', 'link[]');
		row.find('.iframe-input').attr('name', 'iframe[]');
		row.find('.copy-input').attr('name', 'copy[]');
		row.find('.toggle-media').click(function(e){
			e.preventDefault();
			var that = $(this);

			that.nextAll('.media-image, label').toggleClass('hidden');
			that.nextAll('.media-image').attr('src', templateUrl + '/images/gallery-no-image.jpg');
			that.nextAll('.custom-media-url').html('');
			that.nextAll('label').find('.iframe-input').val('');
		});

		row.find('.media-image').click(function(e){
			e.preventDefault();
			makeMediaGallery(this);
		});

		row.find('.remove-row').on('click', function(){
			$(this).parents('tr').remove();
			return false;
		});

		row.insertBefore('#repeatable-fieldset-one tbody>tr:last');
		row.find('input[class*="input"], textarea[class*="input"]').each(function(){
			var that = $(this);

			that.attr('id', that.attr('id').replace('new', row.index() + 1));

			if(typeof qTranslateConfig === 'object'){
				qTranslateConfig.qtx.addContentHooks(that);
			}
		});

		return false;
	});

	/**
	 * Removes a row from the gallery manager
	 */
	$('.remove-row').on('click', function(){
		$(this).parents('tr').remove();

		return false;
	});

	/**
	 * Just runs the media manager opener when you click an image
	 */
	$('.media-image').click(function(e){
		e.preventDefault();
		makeMediaGallery(this);
	});

	$('.toggle-media').click(function(e){
		e.preventDefault();
		var that = $(this);

		that.nextAll('.media-image, label').toggleClass('hidden');
		that.nextAll('.media-image').attr('src', templateUrl + '/images/gallery-no-image.jpg');
		that.nextAll('.custom-media-url').html('');
		that.nextAll('label').find('.iframe-input').val('');
	});

	/**
	 * Makes an ajax post to the save_events function
	 * @param  {obj} data  The form data
	 * @param  {fn} _callback Callback function
	 * @return {fn}           The callback
	 */
	$('.save-event-copy').click(function(e){
		var data = {
			action: 'event_info_save',
			data: {
				event_list_meta_box_nonce: $('#event_list_meta_box_nonce').val(),
				events_copy: $('#events_copy').val()
			}
		};

		jQuery.ajax({
			type: 'POST',
			url: siteUrl + '/wp-admin/admin-ajax.php',
			data: data,
			success: function(data){
				location.reload();
			}
		});
	});

	/**
	 * Makes an ajax post to the save_events function
	 * @param  {obj} data  The form data
	 * @param  {fn} _callback Callback function
	 * @return {fn}           The callback
	 */
	$('.save-tour-copy').click(function(e){
		var data = {
			action: 'tour_info_save',
			data: {
				tour_list_meta_box_nonce: $('#tour_list_meta_box_nonce').val(),
				tours_copy: $('#tours_copy').val()
			}
		};

		jQuery.ajax({
			type: 'POST',
			url: siteUrl + '/wp-admin/admin-ajax.php',
			data: data,
			success: function(data){
				location.reload();
			}
		});
	});

	$('.save-unit-info').on('click', function(e){
		e.preventDefault();
		if(typeof qTranslateConfig === 'object'){
			var allUnitsInputs = $('input[name^="qtranslate-fields[all_unit_info]"]'),
				singleUnitsInputs = $('input[name^="qtranslate-fields[single_unit_info]"]'),
				allUnitsContent = '[:]',
				singleUnitsContent = '{:}',
				data;

			allUnitsInputs.each(function(index){
				if(index === allUnitsInputs.length - 1) return;
				allUnitsContent = $(this).attr('name').replace(/.*(?=\[([a-zA-z_]+?)]*$).*/, '[:$1]') + $(this).val() + allUnitsContent;
			});

			singleUnitsInputs.each(function(index){
				if(index === singleUnitsInputs.length - 1) return;
				singleUnitsContent = $(this).attr('name').replace(/.*(?=\[([a-zA-z_]+?)]*$).*/, '{:$1}') + $(this).val() + singleUnitsContent;
			});

			data = {
				unit_info_meta_box_nonce: $('#unit_info_meta_box_nonce').val(),
				all_unit_info: allUnitsContent,
				single_unit_info: singleUnitsContent
			};

			saveUnitInfo(data);
		}else{
			saveUnitInfo(urlParams($('#unit_info_meta_box_nonce, #all_unit_info, #single_unit_info').serialize()));
		}
	});

	$('#unit-list td span').click(function(e){
		e.preventDefault();
		var that = $(this),
			input = that.next('input');

		input.focus(function(){
			var val = input.val();

			$(window).on('keydown', function(e){
				if(e.which === 27){
					e.preventDefault();
					input.val(val).blur();
					$(window).off('keydown');
				}

				if(e.which === 13){
					e.preventDefault();
					input.blur();
					$(window).off('keydown');
				}
			});
		});

		input.blur(function(){
			input.hide();
			that.show();
		});

		that.hide().next('input').show().focus();

		input.change(function(){
			var rowId = input.closest('tr').data('id'),
				displayVal = input.val(),
				data = {
					column: input.attr('id').split('-')[0],
					unit_info_meta_box_nonce: $('#unit_info_meta_box_nonce').val(),
					row_id: rowId
				};

			if(input.attr('id').indexOf('affordable') >= 0){
				displayVal = input.prop('checked') ? 'Yes' : 'No';
				input.val(input.prop('checked') ? 1 : 0);
			}else if(input.attr('id').indexOf('SqFt') >= 0){
				displayVal += ' ft.<sup>2</sup>';
			}else if(input.attr('id').indexOf('price') >= 0){
				displayVal = '$' + displayVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			}
			if(input.attr('type') === 'number'){
				data.value = parseInt(input.val(), 10);
			}else{
				data.value = input.val();
			}

			that.html(displayVal);
			updateUnitEntry(data);
		});
	});

	$('#unit-list td.available input[type="checkbox"]').change(function(){
		var that = $(this),
			rowId = that.closest('tr').data('id'),
			data = {
				column: that.attr('id').split('-')[0],
				unit_info_meta_box_nonce: $('#unit_info_meta_box_nonce').val(),
				row_id: rowId,
				value: that.prop('checked') ? 1 : 0
			};

		updateUnitEntry(data);
	});
});

/**
 * this updates the qtrans fields on blur, but they dont exist until well after doc.ready
 */
jQuery(window).on('load', function(){
	if(typeof qTranslateConfig === 'object'){
		$('.qtranxs-translatable').change(function(){
			var that = jQuery(this);

			that.prevAll('input[name="qtranslate-fields[' + that.attr('name') + '][' + qTranslateConfig.activeLanguage + ']"]').val(that.val());
		});
	}
});
