/* global site: false, app: false, urlParams: false */

/**
 * Changes the url back when a modal is closed
 */
app.afterCloseModal = function(){
	if(history.replaceState){
		var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

		window.history.replaceState({ path: newUrl }, '', newUrl);
	}
};

/**
 * if there is a "u" (unit) query string, show a modal. also do this on history change (back/forward)
 */
app.afterLoad = window.onpopstate = function(){
	var location = urlParams();

	if(location.u){
		var url = site.url + '/units/' + location.u + '?modal',
			target = 'section.unit-section';

		app.makeModals('content-modal');
		$.get(url, function(data){
			if($(data).find(target).length){
				app.modalContent.html($(data).find(target));
				app.observeIntersections(app.modal[0]);
			}else{
				if(app.modalOpen){
					app.modalClose();
				}
			}
		}).fail(function(){
			if(app.modalOpen){
				app.modalClose();
			}
		});
	}else{
		if(app.modalOpen){
			app.modalClose();
		}
	}
};

$(document).ready(function(){
	/**
	 * Changes the url when a modal is opened so the modal link can be copied
	 */
	$('.unit a').click(function(){
		if(history.pushState){
			var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?u=' + $(this).data('unit');

			window.history.pushState({ path: newUrl }, '', newUrl);
		}
	});

	/**
	 * listens for arrow keys when a unit has focus. this allows for navigating the buildings without a mouse
	 */
	$('.unit a').focus(function(){
		var that = $(this);

		that.on('keydown', function(e){
			var index;

			if(e.which === 37){ // left
				e.preventDefault();
				if(!that.parent().is(':first-child')){
					that.parent().prevAll('.unit').first().find('a').focus();
					that.off('keydown');
				}else if(!that.closest('.floor').is(':first-child')){
					that.closest('.floor').prev().children().last().find('a').focus();
					that.off('keydown');
				}else if(!that.closest('.building').parent().is(':first-child')){
					that.closest('.building').parent().prev().children('.building').last().children('.floor').last().children('.unit').last().find('a').focus();
					that.off('keydown');
				}
			}

			if(e.which === 39){ // right
				e.preventDefault();
				if(!that.parent().is(':last-child')){
					that.parent().nextAll('.unit').first().find('a').focus();
					that.off('keydown');
				}else if(!that.closest('.floor').is(':last-child')){
					that.closest('.floor').next().children().first().find('a').focus();
					that.off('keydown');
				}else if(!that.closest('.building').parent().is(':last-child')){
					that.closest('.building').parent().next().children('.building').first().children('.floor').first().children('.unit').first().find('a').focus();
					that.off('keydown');
				}
			}

			if(e.which === 38){ // up
				e.preventDefault();
				index = that.parent().index() + 1 > that.closest('.floor').prev().children().length ? that.closest('.floor').prev().children().length - 1 : that.parent().index();
				if(!that.closest('.floor').is(':first-child')){
					that.closest('.floor').prev().children(':eq(' + index + ')').find('a').focus();
					that.off('keydown');
				}
			}

			if(e.which === 40){ // down
				e.preventDefault();
				index = that.parent().index() + 1 > that.closest('.floor').next().children().length ? that.closest('.floor').next().children().length - 1 : that.parent().index();
				if(!that.closest('.floor').is(':last-child')){
					that.closest('.floor').next().children(':eq(' + index + ')').find('a').focus();
					that.off('keydown');
				}
			}

			if(e.which === 13){ // enter
				/**
				 * Changes the url when a modal is opened so the modal link can be shared
				 */
				if(history.pushState){
					var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?u=' + $(this).data('unit');

					window.history.pushState({ path: newUrl }, '', newUrl);
				}
				that.off('keydown');
			}
		});
	});
});
