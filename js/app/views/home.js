/* global site: false, win: true, urlParams: false, __: false, app: true */

/**
 * Makes an ajax call to get the posts for the given date
 * @param  {string} _category category slug to fetch
 * @param  {object} that      element scope to apply to
 */
function getPosts(_category, that){
	scrollTo('html');
	$('#post-list').html($('#modal-content').html()).addClass('loading');
	$.get(window.location.href.split('?')[0] + '?category=' + _category, function(data){
		$('#post-list').html($(data).find('#post-list').html()).removeClass('loading');
		$('#post-list .post-category').click(function(e){
			that = $(this);
			e.preventDefault();

			$('#post-categories .post-category[data-category="' + that.data('category') + '"]').addClass('selected').siblings().removeClass('selected');
			$('#post-list').html($('#modal-content').html());
			_category = that.data('category');

			getPosts(_category, that);
		});
		app.observeIntersections();
	});
}

$(document).ready(function(){
	var _category = urlParams().category || '';
	$('#post-categories .post-category[data-category="' + _category + '"]').addClass('selected').siblings().removeClass('selected');

	$('.post-category').click(function(e){
		var that = $(this);

		e.preventDefault();
		$('#post-categories .post-category[data-category="' + that.data('category') + '"]').addClass('selected').siblings().removeClass('selected');
		_category = that.data('category');

		getPosts(_category, that);

		$('header h1').html(that.data('category') ? __('Posts for category: %s', that.text(), 'mvnp_basic') : __('posts', 'mvnp_basic'));
	});
});
