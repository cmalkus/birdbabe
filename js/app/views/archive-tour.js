/* global site: false, win: true, urlParams: false, __: false, app: true */

/**
 * Makes an ajax call to get the tours for the given date
 * @param  {string} _island island slug to fetch
 * @param  {object} that      element scope to apply to
 */
function getTours(_island, _type, that){
	scrollTo('html');
	$('#tour-list').html($('#modal-content').html()).addClass('loading');
	$.get(window.location.href.split('?')[0] + '?island=' + _island + '&type=' + _type, function(data){
		$('#tour-list').html($(data).find('#tour-list').html()).removeClass('loading');
		$('#tour-list .tour-island, #tour-list .tour-type').click(function(e){
			that = $(this);
			e.preventDefault();

			$('#tour-categories .tour-island[data-island="' + that.data('island') + '"]').addClass('selected').siblings().removeClass('selected');
			$('#tour-types .tour-type[data-type="' + that.data('type') + '"]').addClass('selected').siblings().removeClass('selected');

			$('#tour-list').html($('#modal-content').html());
			_island = that.data('category');

			getTours(_island, _type, that);
		});
		app.observeIntersections();
	});
}

$(document).ready(function(){
	var _island = urlParams().island || '';
	var _type = urlParams().type || '';

	$('#tour-categories .tour-island[data-category="' + _island + '"]').addClass('selected').siblings().removeClass('selected');
	$('#tour-types .tour-type[data-category="' + _type + '"]').addClass('selected').siblings().removeClass('selected');

	$('.tour-island').click(function(e){
		var that = $(this);

		e.preventDefault();
		$('#tour-categories .tour-island[data-category="' + that.data('category') + '"]').addClass('selected').siblings().removeClass('selected');
		_island = that.data('category');

		getTours(_island, _type, that);

		$('header h1').html(that.data('category') ? __('Tours for %s', that.text(), 'mvnp_basic') : __('tours', 'mvnp_basic'));
	});

	$('.tour-type').click(function(e){
		var that = $(this);

		e.preventDefault();
		$('#tour-types .tour-type[data-category="' + that.data('category') + '"]').addClass('selected').siblings().removeClass('selected');
		_type = that.data('category');

		getTours(_island, _type, that);

		$('header h1').html(that.data('category') ? __('Tours for %s', that.text(), 'mvnp_basic') : __('tours', 'mvnp_basic'));
	});
});
