/* global app: false, urlParams: false, __: false, site: false, post: false */
/**
 * formats us phone numbers into (area code) phone-number format
 * @param  {string} num phone number to format
 * @return {string}     formatted number
 */
function formatPhoneNumber(num){
	if(!num) return;

	var str = num.toString();
	var matched = str.match(/\d+\.?\d*/g);

	if(!matched) return num;
	// 10 digit
	if(matched.length === 3){
		return '(' + matched[0] + ') ' + matched[1] + '-' + matched[2];
		// 7 digit
	}else if(matched.length === 2){
		return matched[0] + '-' + matched[1];
	}else if(matched.length === 1){
		// no formatting attempted only found integers (i.e. 1234567890)
		// 10 digit
		if(matched[0].length === 10){
			return '(' + matched[0].substr(0, 3) + ') ' + matched[0].substr(3, 3) + '-' + matched[0].substr(6);
		}
		// 7 digit
		if(matched[0].length === 7){
			return matched[0].substr(0, 3) + '-' + matched[0].substr(3);
		}
	}

	// Format failed, return number back
	return num;
}

$(document).ready(function(){
	$('#tour-slider').bxSlider({
		useCSS: false
	});

	/**
	 * Creates the signup modal and from submition for the signup
	 */
	$('.specialty-tour-signup-link').click(function(e){
		var loading = $('#modal-content').html();

		e.preventDefault();

		app.makeModals('signup-modal');
		app.modalContent.load(site.theme.uri + '/views/specialty-tour-form.twig' + '?_=' + (new Date()).getTime(), function(){
			$('#private-form input#first-name').focus();

			$('#tour-name').val(post.post_title);
			$('#tour-date').val(post.custom.datetime);
			$('#private-form h3').text(__('Sign up for %s', post.post_title, 'mvnp_basic'));

			$('input[type="tel"]').blur(function(){
				$(this).val(formatPhoneNumber($(this).val()));
			});

			$('#private-form').submit(function(event){
				var self = $(this),
					submitButton = $('#private-submit'),
					errors = [],
					phoneRe = /((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: ?|\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/,
					emailRe = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
					data = {
						action: 'submit_private',
						data: urlParams(self.serialize())
					};

				event.preventDefault();
				self.append('<div id="submit-loading">' + loading + '</div>');

				var loadingOverlay = $('#submit-loading');

				submitButton.prop('disabled', true);

				self.find('[required]').each(function(){
					var that = $(this);
					that.removeClass('error');
					that.nextAll('span').text('');

					if(!that.val()){
						errors.push(__('You are missing a required field', 'mvnp_basic'));
						that.addClass('error');
						that.nextAll('span').text(__('This field is required', 'mvnp_basic'));
					}else if(that.attr('type') === 'email' && !emailRe.test(that.val())){
						errors.push(__('Invalid Email Address', 'mvnp_basic'));
						that.addClass('error');
						that.nextAll('span').text(__('Invalid Email Address', 'mvnp_basic'));
					}else if(that.attr('type') === 'tel' && !phoneRe.test(that.val())){
						errors.push(__('Please enter a valid phone number', 'mvnp_basic'));
						that.addClass('error');
						that.nextAll('span').text(__('Please enter a valid phone number', 'mvnp_basic'));
					}
				}).promise().done(function(){
					if(errors.length < 1){
						$.ajax({
							type: 'POST',
							url: app.ajaxUrl,
							data: data,
							success: function(data){
								if(parseInt(data, 10) !== 0){
									if(data.status > 0){
										var out;
										data = data.data;

										out = '<h3>' + __('Thank You!', 'mvnp_basic') + '</h3>';
										out += '<p>' + __('Your interest has been registered and and email has been sent! I will get back to you within two business days and we can begin our birding adventure!', 'mvnp_basic') + '</p>';

										self.html(out);
									}else{
										app.notification(data.errors, 'error');
										loadingOverlay.remove();
										submitButton.removeAttr('disabled');
									}
								}else{
									app.notification(__('Internal error. Please try again.', 'mvnp_basic'), 'error');
									loadingOverlay.remove();
									submitButton.removeAttr('disabled');
								}
							}
						});
					}else{
						app.notification(errors, 'error');
						loadingOverlay.remove();
						submitButton.removeAttr('disabled');
					}
				});
			});
		});
	});
});
