<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();
$post = Timber::query_post();
$context['post'] = $post;

$context['sidebar'] = Timber::get_sidebar('single-tour-sidebar.php', $context);
Timber::render('single-tour.twig', $context);
