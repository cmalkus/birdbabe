<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

global $wp;

$args = array(
	'post_type' => 'specialty-tour',
	'posts_per_page' => -1,
	'orderby' => '',
	'order' => 'ASC',
);

$context = Timber::context();
$context['post'] = new stdClass();
$context['title'] = __('Specialty Tours', 'mvnp_basic');
$context['post']->post_content = sprintf(__('Upcoming specialty tours for %s', 'mvnp_basic'), get_bloginfo('name'));
$context['post']->title = $context['title'];
$context['post']->link = home_url(add_query_arg(array(), $wp->request));
$context['post']->type = 'page';
$context['tours'] = new Timber\PostQuery($args);

Timber::render('archive-specialty-tour.twig', $context);
